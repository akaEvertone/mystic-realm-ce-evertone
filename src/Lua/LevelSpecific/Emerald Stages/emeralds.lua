--Xian.exe was here
addHook("PlayerThink", function(p)
	if gamemap == 123 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD1) ~= 1) then
		emeralds = $ + 1
		if not ((marathonmode & MA_NOCUTSCENES) and p.mrce and p.mrce.hud == 1) then
			S_StartSound(nil, sfx_cgot)
		end
	elseif gamemap == 124 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD2) ~= 2) then
		emeralds = $ + 2
		if not ((marathonmode & MA_NOCUTSCENES) and p.mrce and p.mrce.hud == 1) then
			S_StartSound(nil, sfx_cgot)
		end
	elseif gamemap == 125 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD3) ~= 4) then
		emeralds = $ + 4
		if not ((marathonmode & MA_NOCUTSCENES) and p.mrce and p.mrce.hud == 1) then
			S_StartSound(nil, sfx_cgot)
		end
	elseif gamemap == 126 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD4) ~= 8) then
		emeralds = $ + 8
		if not ((marathonmode & MA_NOCUTSCENES) and p.mrce and p.mrce.hud == 1) then
			S_StartSound(nil, sfx_cgot)
		end
	elseif gamemap == 127 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD5) ~= 16) then
		emeralds = $ + 16
		if not ((marathonmode & MA_NOCUTSCENES) and p.mrce and p.mrce.hud == 1) then
			S_StartSound(nil, sfx_cgot)
		end
	elseif gamemap == 128 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD6) ~= 32) then
		emeralds = $ + 32
		if not ((marathonmode & MA_NOCUTSCENES) and p.mrce and p.mrce.hud == 1) then
			S_StartSound(nil, sfx_cgot)
		end
	elseif gamemap == 129 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD7) ~= 64) then
		emeralds = $ + 64
		if not ((marathonmode & MA_NOCUTSCENES) and p.mrce and p.mrce.hud == 1) then
			S_StartSound(nil, sfx_cgot)
		end
	--elseif gamemap == 129 and (p.pflags & PF_FINISHED) and All7Emeralds(emeralds) then
		--S_StartSound(nil, sfx_s3k9c)
		--hyperunlocked = true
	end
end)

--goalring support for netgames
addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid then return end
	--if gamemap < 123 return end //only mudhole karst has a shrine rn, so only it needs this function

	--if gamemap > 129 return end

	if gamemap ~= 123 and gamemap ~= 125 then return end

	if GoalRing == nil then
		P_RemoveMobj(mobj)
	end
end, MT_SIGN)

addHook("TouchSpecial", function(mo, toucher)
	if gamemap >= 123 and gamemap <= 129 then return true end --don't want players interacting with the emerald
	if mapheaderinfo[gamemap].mrce_emeraldstage then return true end
end, MT_EMERALD1)

addHook("TouchSpecial", function(mo, toucher)
	if gamemap >= 123 and gamemap <= 129 then return true end
	if mapheaderinfo[gamemap].mrce_emeraldstage then return true end
end, MT_EMERALD2)

addHook("TouchSpecial", function(mo, toucher)
	if gamemap >= 123 and gamemap <= 129 then return true end
	if mapheaderinfo[gamemap].mrce_emeraldstage then return true end
end, MT_EMERALD3)

addHook("TouchSpecial", function(mo, toucher)
	if gamemap >= 123 and gamemap <= 129 then return true end
	if mapheaderinfo[gamemap].mrce_emeraldstage then return true end
end, MT_EMERALD4)

addHook("TouchSpecial", function(mo, toucher)
	if gamemap >= 123 and gamemap <= 129 then return true end
	if mapheaderinfo[gamemap].mrce_emeraldstage then return true end
end, MT_EMERALD5)

addHook("TouchSpecial", function(mo, toucher)
	if gamemap >= 123 and gamemap <= 129 then return true end
	if mapheaderinfo[gamemap].mrce_emeraldstage then return true end
end, MT_EMERALD6)

addHook("TouchSpecial", function(mo, toucher)
	if gamemap >= 123 and gamemap <= 129 then return true end
	if mapheaderinfo[gamemap].mrce_emeraldstage then return true end
end, MT_EMERALD7)

addHook("MobjThinker", function(mobj)
	if not (mobj and mobj.valid) then return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE) then
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 83)
	end
	if mobj.valid and not (mobj.frame & (FF_TRANS10)) then
		mobj.frame = $|FF_TRANS10
	end
	if mobj.sprite == SPR_CEMG then
		mobj.sprite = SPR_MSTE
	end

	local color = SKINCOLOR_SAPPHIRE
	if mapheaderinfo[gamemap].ltzztext == "MYSTIC1" then
		mobj.frame = J
	else
		mobj.frame = A
		color = SKINCOLOR_APPLE
	end

	if not mobj.overlay then
		mobj.overlay = P_SpawnMobjFromMobj(mobj, 0, 0, 0, MT_OVERLAY)
		mobj.overlay.state = S_INVISIBLE
		mobj.overlay.sprite = SPR_OILF
		mobj.overlay.frame = FF_FULLBRIGHT|FF_TRANS60|FF_ADD
		mobj.overlay.color = color
		mobj.overlay.colorized = true
		mobj.overlay.target = mobj

		local scale = FixedMul(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, mobj.scale)
		mobj.overlay.spritexscale = scale
		mobj.overlay.spriteyscale = scale

		mobj.overlay.spriteyoffset = 32*FRACUNIT
	elseif mobj.overlay then
		if mobj.health > 0 then
			local scale = FixedMul(min(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, 12*FRACUNIT), mobj.scale)
			mobj.overlay.spritexscale = scale
			mobj.overlay.spriteyscale = scale

			mobj.overlay.spriteyoffset = min(FixedDiv(32*FRACUNIT, scale*2), 32*FRACUNIT)
		else
			if states[mobj.state].tics == mobj.tics then
				MRCE_superSpark(mobj, 2, TICRATE, 1, 1*FRACUNIT, false, 83)
				mobj.flags2 = $|MF2_DONTDRAW
			end
			mobj.overlay.spritexscale = ease.outsine(FRACUNIT/4, mobj.overlay.spritexscale, 0)
			mobj.overlay.spriteyscale = mobj.overlay.spritexscale
		end
	end
end, MT_EMERALD1)

addHook("MobjThinker", function(mobj)
	if not mobj and mobj.valid then return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE) then
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 89)
	end
	if mobj.valid and not (mobj.frame & (FF_TRANS10)) then
		mobj.frame = $|FF_TRANS10
	end
	if mobj.sprite == SPR_CEMG then
		mobj.sprite = SPR_MSTE
	end

	if mapheaderinfo[gamemap].ltzztext == "MYSTIC1" then
		mobj.frame = K
	else
		mobj.frame = B
	end

	if not mobj.overlay then
		mobj.overlay = P_SpawnMobjFromMobj(mobj, 0, 0, 0, MT_OVERLAY)
		mobj.overlay.state = S_INVISIBLE
		mobj.overlay.sprite = SPR_OILF
		mobj.overlay.frame = FF_FULLBRIGHT|FF_TRANS60|FF_ADD
		mobj.overlay.color = SKINCOLOR_LAVENDER
		mobj.overlay.colorized = true
		mobj.overlay.target = mobj

		local scale = FixedMul(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, mobj.scale)
		mobj.overlay.spritexscale = scale
		mobj.overlay.spriteyscale = scale

		mobj.overlay.spriteyoffset = 32*FRACUNIT
	elseif mobj.overlay then
		if mobj.health > 0 then
			local scale = FixedMul(min(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, 12*FRACUNIT), mobj.scale)
			mobj.overlay.spritexscale = scale
			mobj.overlay.spriteyscale = scale

			mobj.overlay.spriteyoffset = min(FixedDiv(32*FRACUNIT, scale*2), 32*FRACUNIT)
		else
			if states[mobj.state].tics == mobj.tics then
				MRCE_superSpark(mobj, 2, TICRATE, 1, 1*FRACUNIT, false, 89)
				mobj.flags2 = $|MF2_DONTDRAW
			end
			mobj.overlay.spritexscale = ease.outsine(FRACUNIT/4, mobj.overlay.spritexscale, 0)
			mobj.overlay.spriteyscale = mobj.overlay.spritexscale
		end
	end
end, MT_EMERALD2)

addHook("MobjThinker", function(mobj)
	if not mobj and mobj.valid then return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE) then
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 66)
	end
	if mobj.valid and not (mobj.frame & (FF_TRANS10)) then
		mobj.frame = $|FF_TRANS10
	end
	if mobj.sprite == SPR_CEMG then
		mobj.sprite = SPR_MSTE
	end

	local color = SKINCOLOR_APPLE
	if mapheaderinfo[gamemap].ltzztext == "MYSTIC1" then
		mobj.frame = L
	else
		mobj.frame = C
		color = SKINCOLOR_SAPPHIRE
	end

	if not mobj.overlay then
		mobj.overlay = P_SpawnMobjFromMobj(mobj, 0, 0, 0, MT_OVERLAY)
		mobj.overlay.state = S_INVISIBLE
		mobj.overlay.sprite = SPR_OILF
		mobj.overlay.frame = FF_FULLBRIGHT|FF_TRANS60|FF_ADD
		mobj.overlay.color = color
		mobj.overlay.colorized = true
		mobj.overlay.target = mobj

		local scale = FixedMul(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, mobj.scale)
		mobj.overlay.spritexscale = scale
		mobj.overlay.spriteyscale = scale

		mobj.overlay.spriteyoffset = 32*FRACUNIT
	elseif mobj.overlay then
		if mobj.health > 0 then
			local scale = FixedMul(min(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, 12*FRACUNIT), mobj.scale)
			mobj.overlay.spritexscale = scale
			mobj.overlay.spriteyscale = scale

			mobj.overlay.spriteyoffset = min(FixedDiv(32*FRACUNIT, scale*2), 32*FRACUNIT)
		else
			if states[mobj.state].tics == mobj.tics then
				MRCE_superSpark(mobj, 2, TICRATE, 1, 1*FRACUNIT, false, 66)
				mobj.flags2 = $|MF2_DONTDRAW
			end
			mobj.overlay.spritexscale = ease.outsine(FRACUNIT/4, mobj.overlay.spritexscale, 0)
			mobj.overlay.spriteyscale = mobj.overlay.spritexscale
		end
	end
end, MT_EMERALD3)

addHook("MobjThinker", function(mobj)
	if not mobj and mobj.valid then return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE) then
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 36)
	end
	if mobj.valid and not (mobj.frame & (FF_TRANS10)) then
		mobj.frame = $|FF_TRANS10
	end
	if mobj.sprite == SPR_CEMG then
		mobj.sprite = SPR_MSTE
	end

	local color = SKINCOLOR_SALMON
	if mapheaderinfo[gamemap].ltzztext == "MYSTIC1" then
		mobj.frame = M
	else
		mobj.frame = D
		color = SKINCOLOR_ARCTIC
	end

	if not mobj.overlay then
		mobj.overlay = P_SpawnMobjFromMobj(mobj, 0, 0, 0, MT_OVERLAY)
		mobj.overlay.state = S_INVISIBLE
		mobj.overlay.sprite = SPR_OILF
		mobj.overlay.frame = FF_FULLBRIGHT|FF_TRANS60|FF_ADD
		mobj.overlay.color = color
		mobj.overlay.colorized = true
		mobj.overlay.target = mobj

		local scale = FixedMul(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, mobj.scale)
		mobj.overlay.spritexscale = scale
		mobj.overlay.spriteyscale = scale

		mobj.overlay.spriteyoffset = 32*FRACUNIT
	elseif mobj.overlay then
		if mobj.health > 0 then
			local scale = FixedMul(min(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, 12*FRACUNIT), mobj.scale)
			mobj.overlay.spritexscale = scale
			mobj.overlay.spriteyscale = scale

			mobj.overlay.spriteyoffset = min(FixedDiv(32*FRACUNIT, scale*2), 32*FRACUNIT)
		else
			if states[mobj.state].tics == mobj.tics then
				MRCE_superSpark(mobj, 2, TICRATE, 1, 1*FRACUNIT, false, 36)
				mobj.flags2 = $|MF2_DONTDRAW
			end
			mobj.overlay.spritexscale = ease.outsine(FRACUNIT/4, mobj.overlay.spritexscale, 0)
			mobj.overlay.spriteyscale = mobj.overlay.spritexscale
		end
	end
end, MT_EMERALD4)

addHook("MobjThinker", function(mobj)
	if not mobj and mobj.valid then return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE) then
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 76)
	end
	if mobj.valid and not (mobj.frame & (FF_TRANS10)) then
		mobj.frame = $|FF_TRANS10
	end
	if mobj.sprite == SPR_CEMG then
		mobj.sprite = SPR_MSTE
	end

	local color = SKINCOLOR_ARCTIC
	if mapheaderinfo[gamemap].ltzztext == "MYSTIC1" then
		mobj.frame = N
	else
		mobj.frame = E
		color = SKINCOLOR_SUNSET
	end

	if not mobj.overlay then
		mobj.overlay = P_SpawnMobjFromMobj(mobj, 0, 0, 0, MT_OVERLAY)
		mobj.overlay.state = S_INVISIBLE
		mobj.overlay.sprite = SPR_OILF
		mobj.overlay.frame = FF_FULLBRIGHT|FF_TRANS60|FF_ADD
		mobj.overlay.color = color
		mobj.overlay.colorized = true
		mobj.overlay.target = mobj

		local scale = FixedMul(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, mobj.scale)
		mobj.overlay.spritexscale = scale
		mobj.overlay.spriteyscale = scale

		mobj.overlay.spriteyoffset = 32*FRACUNIT
	elseif mobj.overlay then
		if mobj.health > 0 then
			local scale = FixedMul(min(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, 12*FRACUNIT), mobj.scale)
			mobj.overlay.spritexscale = scale
			mobj.overlay.spriteyscale = scale

			mobj.overlay.spriteyoffset = min(FixedDiv(32*FRACUNIT, scale*2), 32*FRACUNIT)
		else
			if states[mobj.state].tics == mobj.tics then
				MRCE_superSpark(mobj, 2, TICRATE, 1, 1*FRACUNIT, false, 76)
				mobj.flags2 = $|MF2_DONTDRAW
			end
			mobj.overlay.spritexscale = ease.outsine(FRACUNIT/4, mobj.overlay.spritexscale, 0)
			mobj.overlay.spriteyscale = mobj.overlay.spritexscale
		end
	end
end, MT_EMERALD5)

addHook("MobjThinker", function(mobj)
	if not mobj and mobj.valid then return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE) then
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 56)
	end
	if mobj.valid and not (mobj.frame & (FF_TRANS10)) then
		mobj.frame = $|FF_TRANS10
	end
	if gamemap == 128 then
		if GoalRing == nil then
			P_RemoveMobj(mobj)
		end
	end
	if mobj.sprite == SPR_CEMG then
		mobj.sprite = SPR_MSTE
	end

	local color = SKINCOLOR_SUNSET
	if mapheaderinfo[gamemap].ltzztext == "MYSTIC1" then
		mobj.frame = O
	else
		mobj.frame = F
		color = SKINCOLOR_SALMON
	end

	if not mobj.overlay then
		mobj.overlay = P_SpawnMobjFromMobj(mobj, 0, 0, 0, MT_OVERLAY)
		mobj.overlay.state = S_INVISIBLE
		mobj.overlay.sprite = SPR_OILF
		mobj.overlay.frame = FF_FULLBRIGHT|FF_TRANS60|FF_ADD
		mobj.overlay.color = color
		mobj.overlay.colorized = true
		mobj.overlay.target = mobj

		local scale = FixedMul(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, mobj.scale)
		mobj.overlay.spritexscale = scale
		mobj.overlay.spriteyscale = scale

		mobj.overlay.spriteyoffset = 32*FRACUNIT
	elseif mobj.overlay then
		if mobj.health > 0 then
			local scale = FixedMul(min(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, 12*FRACUNIT), mobj.scale)
			mobj.overlay.spritexscale = scale
			mobj.overlay.spriteyscale = scale

			mobj.overlay.spriteyoffset = min(FixedDiv(32*FRACUNIT, scale*2), 32*FRACUNIT)
		else
			if states[mobj.state].tics == mobj.tics then
				MRCE_superSpark(mobj, 2, TICRATE, 1, 1*FRACUNIT, false, 56)
				mobj.flags2 = $|MF2_DONTDRAW
			end
			mobj.overlay.spritexscale = ease.outsine(FRACUNIT/4, mobj.overlay.spritexscale, 0)
			mobj.overlay.spriteyscale = mobj.overlay.spritexscale
		end
	end
end, MT_EMERALD6)

addHook("MobjThinker", function(mobj)
	if not mobj and mobj.valid then return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE) then
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 11)
	end
	if mobj.valid and not (mobj.frame & (FF_TRANS10)) then
		mobj.frame = $|FF_TRANS10
	end
	if mobj.sprite == SPR_CEMG then
		mobj.sprite = SPR_MSTE
	end
	if mapheaderinfo[gamemap].ltzztext == "MYSTIC1" then
		mobj.frame = P
	else
		mobj.frame = G
	end
	if not mobj.overlay then
		mobj.overlay = P_SpawnMobjFromMobj(mobj, 0, 0, 0, MT_OVERLAY)
		mobj.overlay.state = S_INVISIBLE
		mobj.overlay.sprite = SPR_OILF
		mobj.overlay.frame = FF_FULLBRIGHT|FF_TRANS60|FF_ADD
		mobj.overlay.color = SKINCOLOR_BONE
		mobj.overlay.colorized = true
		mobj.overlay.target = mobj

		local scale = FixedMul(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, mobj.scale)
		mobj.overlay.spritexscale = scale
		mobj.overlay.spriteyscale = scale

		mobj.overlay.spriteyoffset = 32*FRACUNIT
	elseif mobj.overlay then
		if mobj.health > 0 then
			local scale = FixedMul(min(FRACUNIT/4 + (R_PointToDist(mobj.x, mobj.y) or 1)/6096, 12*FRACUNIT), mobj.scale)
			mobj.overlay.spritexscale = scale
			mobj.overlay.spriteyscale = scale

			mobj.overlay.spriteyoffset = min(FixedDiv(32*FRACUNIT, scale*2), 32*FRACUNIT)
		else
			if states[mobj.state].tics == mobj.tics then
				MRCE_superSpark(mobj, 2, TICRATE, 1, 1*FRACUNIT, false, 11)
				mobj.flags2 = $|MF2_DONTDRAW
			end
			mobj.overlay.spritexscale = ease.outsine(FRACUNIT/4, mobj.overlay.spritexscale, 0)
			mobj.overlay.spriteyscale = mobj.overlay.spritexscale
		end
	end
end, MT_EMERALD7)