--Tempest Valley Lightning Strikes
freeslot("MT_LIGHTNING_BOLT_PART", "MT_LIGHTNING_SPAWNER", "S_LIGHTNING_BOLT_PART")

mobjinfo[MT_LIGHTNING_SPAWNER] = {
--$Name Lightning Bolt Spawner
--$Sprite THOKA0
--$Category Mystic Realm Special
--$NotAngled
--$Arg0 Radius
--$Arg0RenderStyle Rectangle
--$Arg1 Min Spawntime
--$Arg2 Max Spawntime
--$Arg3 Instakill?
--$Arg4 Bolts
	doomednum = 2223,
	spawnstate = S_INVISIBLE,
	radius = 12*FRACUNIT,
	height = 24*FRACUNIT,
	flags = MF_NOGRAVITY
}

mobjinfo[MT_LIGHTNING_BOLT_PART] = {
	doomednum = -1,
	spawnstate = S_LIGHTNING_BOLT_PART,
	spawnhealth = 1000,
	reactiontime = 8,
	radius = 72*FRACUNIT,
	height = 80*FRACUNIT,
	mass = 16,
	dispoffset = 1,
	flags = MF_SPECIAL|MF_NOGRAVITY
}

states[S_LIGHTNING_BOLT_PART] = {SPR_THOK,A|TR_TRANS50,-1,nil,7,2,S_LIGHTNING_BOLT_PART}

local zapsafe = function(p)
    if (p.powers[pw_shield] & SH_PROTECTELECTRIC) then return true end
    if p.mo.skin == "surge" then return true end
end

addHook("TouchSpecial", function(mo, toucher)
    local KIIEELLL = 0
    if mo.superzap and not zapsafe(toucher.player) then
        KIIEELLL = DMG_INSTAKILL
    end
    P_DamageMobj(toucher, mo, mo, 1, DMG_ELECTRIC|KIIEELLL)
    return true
end, MT_LIGHTNING_BOLT_PART)

addHook("MobjThinker", function(mo)
    if mo.z > mo.floorz + 96*FRACUNIT then
        local spawned = P_SpawnMobjFromMobj(mo, 0, 0, -96*FRACUNIT, MT_LIGHTNING_BOLT_PART)
        spawned.fuse = 10
        spawned.blendmode = AST_ADD
        spawned.color = SKINCOLOR_WHITE
        if mo.superzap then
            spawned.superzap = true
        end
    end

end, MT_LIGHTNING_BOLT_PART)

addHook("MobjThinker", function(mo)
    if not (mo and mo.valid) then return end
    local ag = mo.spawnpoint.args
    mo.boltspawntime = $ or 0
    if mo.boltspawntime > 0 then
        mo.boltspawntime = $ - 1
    else
        for i = 1, max(1, ag[4]), 1 do
            local bx, by = P_RandomRange(-ag[0], ag[0]), P_RandomRange(-ag[0], ag[0])
            local bz = P_CeilingzAtPos(bx, by, mo.z, 24*FRACUNIT)
            local spawned = P_SpawnMobjFromMobj(mo, bx*16*FRACUNIT, by*16*FRACUNIT, bz, MT_LIGHTNING_BOLT_PART)
            S_StartSound(spawned, P_RandomRange(sfx_litng2, sfx_litng4))
            spawned.fuse = 10
            spawned.blendmode = AST_ADD
            spawned.color = SKINCOLOR_WHITE
            if ag[3] and ag[3] > 0 then
                spawned.superzap = true
            end
        end
        mo.boltspawntime = P_RandomRange(ag[1], ag[2])*10
    end
end, MT_LIGHTNING_SPAWNER)

freeslot(
	"MT_LANTERN",
	"S_LCORONA",
	"S_LANTERN",
	"S_LANTERN1",
	"S_LANTERN2",
	"S_LANTERNPOP1",
	"S_LANTERNPOP2",
	"S_LANTERNPOP3",
	"S_LANTERNPOP4",
	"S_LANTERNPOP5",
	"S_LANTERNPOP6",
	"SPR_TARD"
)

local COLOR_LUT = {
SKINCOLOR_WHITE,
SKINCOLOR_BONE,
SKINCOLOR_CLOUDY,
SKINCOLOR_GREY,
SKINCOLOR_SILVER,
SKINCOLOR_CARBON,
SKINCOLOR_JET,
SKINCOLOR_BLACK,
SKINCOLOR_AETHER,
SKINCOLOR_SLATE,
SKINCOLOR_BLUEBELL,
SKINCOLOR_PINK,
SKINCOLOR_YOGURT,
SKINCOLOR_BROWN,
SKINCOLOR_BRONZE,
SKINCOLOR_TAN,
SKINCOLOR_BEIGE,
SKINCOLOR_MOSS,
SKINCOLOR_AZURE,
SKINCOLOR_LAVENDER,
SKINCOLOR_RUBY,
SKINCOLOR_SALMON,
SKINCOLOR_RED,
SKINCOLOR_CRIMSON,
SKINCOLOR_FLAME,
SKINCOLOR_KETCHUP,
SKINCOLOR_PEACHY,
SKINCOLOR_QUAIL,
SKINCOLOR_SUNSET,
SKINCOLOR_COPPER,
SKINCOLOR_APRICOT,
SKINCOLOR_ORANGE,
SKINCOLOR_RUST,
SKINCOLOR_GOLD,
SKINCOLOR_SANDY,
SKINCOLOR_YELLOW,
SKINCOLOR_OLIVE,
SKINCOLOR_LIME,
SKINCOLOR_PERIDOT,
SKINCOLOR_APPLE,
SKINCOLOR_GREEN,
SKINCOLOR_FOREST,
SKINCOLOR_EMERALD,
SKINCOLOR_MINT,
SKINCOLOR_SEAFOAM,
SKINCOLOR_AQUA,
SKINCOLOR_TEAL,
SKINCOLOR_WAVE,
SKINCOLOR_CYAN,
SKINCOLOR_SKY,
SKINCOLOR_CERULEAN,
SKINCOLOR_ICY,
SKINCOLOR_SAPPHIRE,
SKINCOLOR_CORNFLOWER,
SKINCOLOR_BLUE,
SKINCOLOR_COBALT,
SKINCOLOR_VAPOR,
SKINCOLOR_DUSK,
SKINCOLOR_PASTEL,
SKINCOLOR_PURPLE,
SKINCOLOR_BUBBLEGUM,
SKINCOLOR_MAGENTA,
SKINCOLOR_NEON,
SKINCOLOR_VIOLET,
SKINCOLOR_LILAC,
SKINCOLOR_PLUM,
SKINCOLOR_RASPBERRY,
SKINCOLOR_ROSY
}

local TYPE_LANTERN_LUT = {
	[0] = S_LANTERN,
	S_LANTERN1,
	S_LANTERN2,
}

addHook("MapThingSpawn", function(mo, mt)
	local color_arg = mt.stringargs[0] and
	string.upper(mt.stringargs[0] or '') or nil					-- Color
	local sine_arg = mt.args[0] 								-- Z Range
	local spee_arg = mt.args[1]									-- Z Speed
	local sprr_arg = mt.args[2]									-- Disable Tangibility
	local resp_arg = mt.args[3]									-- Disable Respawn
	local spri_arg = mt.args[4]									-- Sprite Type [0, 1, 2]

	if color_arg and _G[color_arg] and skincolors[_G[color_arg]]
	and skincolors[_G[color_arg]] ~= SKINCOLOR_NONE then
		mo.extravalue1 = _G[color_arg]
	else
		mo.extravalue1 = COLOR_LUT[P_RandomRange(1, #COLOR_LUT)]
		if color_arg then
			print("\x85".."WARNING:".."\x80".."Object #"..#mt.." has invalid skincolor indentificator!")
		end
	end

	if spri_arg == -1 then
		mo.state = TYPE_LANTERN_LUT[P_RandomRange(0, #TYPE_LANTERN_LUT)]
	elseif spri_arg < -1 then
		mo.state = TYPE_LANTERN_LUT[P_RandomRange(1, #TYPE_LANTERN_LUT)]
	else
		mo.state = TYPE_LANTERN_LUT[min(max(spri_arg, 0), #TYPE_LANTERN_LUT)]
	end

	mo.disrespawn = (resp_arg == 1)
	mo.extravalue2 = sine_arg >> 2
	mo.speed = spee_arg*(ANG1 >> 2)

	if sprr_arg then
		mo.flags = $ &~ MF_SPRING
	end
end, MT_LANTERN)

addHook("MobjThinker", function(mo)
	if mo.overlay and mo.overlay.valid and (mo.flags2 & MF2_DONTDRAW) then
		mo.overlay.flags2 = $ | MF2_DONTDRAW
	end

	if mo.extravalue2 then
		mo.z = mo.z+(mo.extravalue2*sin(mo.threshold))*P_MobjFlip(mo)
		if mo.hasbeencolored then
			mo.threshold = $+mo.speed
		end
	end

	if mo.disrespawn and mo.state == S_LANTERNPOP4 then
		P_RemoveMobj(mo)
		return
	end

	if mo.hasbeencolored then return false end

	if udmf and not mo.extravalue2 and mo.spawnpoint and mo.spawnpoint.valid then
		local sine_arg = mo.spawnpoint.args[0] 				-- Z Range
		local spee_arg = mo.spawnpoint.args[1] 				-- Z Speed
		local sprr_arg = mo.spawnpoint.args[2]				-- Disable Tangibility
		local spri_arg = mo.spawnpoint.args[4]				-- Sprite Type [0, 1, 2]

		if spri_arg == -1 then
			mo.state = TYPE_LANTERN_LUT[P_RandomRange(0, #TYPE_LANTERN_LUT)]
		elseif spri_arg < -1 then
			mo.state = TYPE_LANTERN_LUT[P_RandomRange(1, #TYPE_LANTERN_LUT)]
		else
			mo.state = TYPE_LANTERN_LUT[min(max(spri_arg, 0), #TYPE_LANTERN_LUT)]
		end
		mo.extravalue2 = sine_arg >> 2
		mo.speed = spee_arg*(ANG1 >> 2)

		if sprr_arg then
			mo.flags = $ &~ MF_SPRING
		end
	end

	mo.shadowscale = (3 << FRACBITS) >> 2
	local myass_corona = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_OVERLAY)
	myass_corona.state = S_LCORONA
	myass_corona.target = mo
	mo.overlay = myass_corona

	if mo.spawnpoint and mo.spawnpoint.options and (mo.spawnpoint.options & MTF_EXTRA) then
		mo.color = COLOR_LUT[P_RandomRange(1, #COLOR_LUT)]
		myass_corona.color = mo.color
		mo.hasbeencolored = true
	else
		if mo.extravalue1 then
			mo.color = mo.extravalue1
		else
			mo.color = max(min(FixedInt(AngleFixed(mo.angle)), #skincolors - 1), 1)
		end
		myass_corona.color = mo.color
		mo.hasbeencolored = true
	end

	mo.angle = FixedAngle(mo.color << FRACBITS)
end, MT_LANTERN)

--addHook("MobjThinker", function(mo)
--	if mo.target and mo.target.valid
--		mo.color = mo.target.color
--		if mo.target.eflags & MFE_VERTICALFLIP then
--			mo.eflags = $ | MFE_VERTICALFLIP
--		else
--			if mo.eflags & MFE_VERTICALFLIP then
--				mo.eflags = $ & ~MFE_VERTICALFLIP
--			end
--		end
--	else
--		P_RemoveMobj(mo)
--	end
--end, MT_LCORONA)

mobjinfo[MT_LANTERN] = {
--$NotAngled
--$Category Tempest Valley
--$Name Lantern
--$Sprite TARDA0
--$Arg0 Z Movement Range
--$Arg1 Z Movement Speed
--$Arg2 Disable Tangibility?
--$Arg2Type 11
--$Arg2Enum noyes
--$Arg3 Disable Respawn?
--$Arg3Type 11
--$Arg3Enum noyes
--$Arg4 Sprite
        doomednum = 69,
        spawnstate = S_LANTERN,
        spawnhealth = 1,
        seestate = S_NULL,
        seesound = sfx_None,
        reactiontime = 0,
        attacksound = sfx_none,
        painstate = S_NULL,
        painchance = MT_THOK,
        painsound = sfx_s3k77,
        meleestate = S_NULL,
        missilestate = S_NULL,
        deathstate = S_LANTERNPOP2,
        xdeathstate = S_NULL,
        deathsound = sfx_None,
        speed = 0,
        radius = 22*FRACUNIT,
        height = 47*FRACUNIT,
        dispoffset = 3,
        mass =17*FRACUNIT,
        damage = 0,
        activesound = sfx_None,
        flags = MF_SPRING|MF_NOGRAVITY|MF_RUNSPAWNFUNC,
        raisestate = S_LANTERNPOP1
}

states[S_LCORONA] = {SPR_TARD, B|FF_SEMIBRIGHT|FF_ADD|FF_TRANS70, -1, nil, 1, 0, S_NULL}
states[S_LANTERN] = {SPR_TARD, A|FF_FULLBRIGHT, -1, A_SetObjectFlags2, MF2_DONTDRAW, 1, S_NULL}
states[S_LANTERN1] = {SPR_TARD, D|FF_FULLBRIGHT, -1, A_SetObjectFlags2, MF2_DONTDRAW, 1, S_NULL}
states[S_LANTERN2] = {SPR_TARD, E|FF_FULLBRIGHT, -1, A_SetObjectFlags2, MF2_DONTDRAW, 1, S_NULL}
states[S_LANTERNPOP1] = {SPR_TARD, A, 0, A_RemoteDamage, 0, 1, S_LANTERNPOP2}
						--
states[S_LANTERNPOP2] = {SPR_NULL, A, 1, A_SetObjectFlags2, MF2_DONTDRAW, 2, S_LANTERNPOP3}
						--
states[S_LANTERNPOP3] = {SPR_NULL, A, 1, A_Pain, 0, 0, S_LANTERNPOP4}
						--
states[S_LANTERNPOP4] = {SPR_NULL, A, 35, A_CheckFlags2, MF2_AMBUSH, 0, S_LANTERNPOP5}
						--
states[S_LANTERNPOP5] = {SPR_NULL, A, 490, nil, 0, 0, S_LANTERNPOP6}
						--
states[S_LANTERNPOP6] = {SPR_NULL, A, 0, A_SpawnFreshCopy, 0, 0, S_NULL}

--
-- Tempest Valley Zone spears
-- by Radicalicious
--

freeslot(
"MT_TVZ_SPEAR_VERTICAL",
"MT_TVZ_SPEAR_HORIZONTAL",
"MT_TVZ_SPEAR_HORIZONTAL_BASE",

"S_TVZ_SPEAR_VERTICAL1",
"S_TVZ_SPEAR_VERTICAL2",
"S_TVZ_SPEAR_VERTICAL3",
"S_TVZ_SPEAR_VERTICAL4",
"S_TVZ_SPEAR_VERTICAL5",
"S_TVZ_SPEAR_VERTICAL6",

"S_TVZ_SPEAR_HORIZONTAL1",
"S_TVZ_SPEAR_HORIZONTAL2",
"S_TVZ_SPEAR_HORIZONTAL3",
"S_TVZ_SPEAR_HORIZONTAL4",
"S_TVZ_SPEAR_HORIZONTAL5",
"S_TVZ_SPEAR_HORIZONTAL6",

"S_TVZ_SPEAR_HORIZONTAL_BASE",

"SPR_TVSK"
)

-- objects: vertical spear
mobjinfo[MT_TVZ_SPEAR_VERTICAL] = {
	--$Name Spear
	--$Sprite TVSKA0
	--$Category Tempest Valley
	--$NotAngled
	--$Arg0 Retraction interval
	--$Arg1 Start interval
	--$Arg2 Flags
	--$Arg2Type 12
	--$Arg2Enum { 1 = "Start retracted"; 2 = "Intangible"; }
	doomednum = 2225,
	spawnstate = S_TVZ_SPEAR_VERTICAL1,
	deathstate = S_NULL,
	meleestate = S_TVZ_SPEAR_VERTICAL4,
	painsound = sfx_s3k64,
	radius = 8*FRACUNIT,
	height = 128*FRACUNIT,
	speed = 2*TICRATE,
	flags = MF_NOGRAVITY|MF_SCENERY|MF_SOLID
}

-- objects: horizontal spear
mobjinfo[MT_TVZ_SPEAR_HORIZONTAL] = {
	--$Name Spear (Horizontal)
	--$Sprite TVSKELER
	--$Category Tempest Valley
	--$Arg0 Retraction interval
	--$Arg1 Start interval
	--$Arg2 Flags
	--$Arg2Type 12
	--$Arg2Enum { 1 = "Start retracted"; 2 = "Intangible"; }
	doomednum = 2226,
	spawnstate = S_TVZ_SPEAR_HORIZONTAL1,
	deathstate = S_NULL,
	meleestate = S_TVZ_SPEAR_HORIZONTAL4,
	painsound = sfx_s3k64,
	radius = 64*FRACUNIT,
	height = 16*FRACUNIT,
	speed = 2*TICRATE,
	flags = MF_NOGRAVITY|MF_SCENERY|MF_SOLID|MF_PAPERCOLLISION|MF_NOCLIPHEIGHT
}
mobjinfo[MT_TVZ_SPEAR_HORIZONTAL_BASE] = {
	doomednum = -1,
	spawnstate = S_TVZ_SPEAR_HORIZONTAL_BASE,
	deathstate = S_NULL,
	radius = 8*FRACUNIT,
	height = 16*FRACUNIT,
	flags = MF_NOGRAVITY|MF_SCENERY|MF_NOCLIP|MF_NOCLIPTHING
}

-- states: vertical spear
states[S_TVZ_SPEAR_VERTICAL1] = {
	sprite = SPR_TVSK,
	frame = FF_SEMIBRIGHT|A,
	tics = -1,
	action = A_SpikeRetract,
	var1 = 1,
	var2 = 0,
	nextstate = S_TVZ_SPEAR_VERTICAL2
}
states[S_TVZ_SPEAR_VERTICAL2] = {
	sprite = SPR_TVSK,
	frame = FF_SEMIBRIGHT|B,
	tics = 2,
	action = A_Pain,
	nextstate = S_TVZ_SPEAR_VERTICAL3
}
states[S_TVZ_SPEAR_VERTICAL3] = {
	sprite = SPR_TVSK,
	frame = FF_SEMIBRIGHT|C,
	tics = 2,
	nextstate = S_TVZ_SPEAR_VERTICAL4
}
states[S_TVZ_SPEAR_VERTICAL4] = {
	sprite = SPR_TVSK,
	frame = FF_SEMIBRIGHT|D,
	tics = -1,
	action = A_SpikeRetract,
	var1 = 0,
	var2 = 0,
	nextstate = S_TVZ_SPEAR_VERTICAL5
}
states[S_TVZ_SPEAR_VERTICAL5] = {
	sprite = SPR_TVSK,
	frame = FF_SEMIBRIGHT|C,
	tics = 2,
	action = A_Pain,
	nextstate = S_TVZ_SPEAR_VERTICAL6
}
states[S_TVZ_SPEAR_VERTICAL6] = {
	sprite = SPR_TVSK,
	frame = FF_SEMIBRIGHT|B,
	tics = 2,
	nextstate = S_TVZ_SPEAR_VERTICAL1
}

-- states: horizontal spear
states[S_TVZ_SPEAR_HORIZONTAL1] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|E,
	tics = -1,
	action = A_SpikeRetract,
	var1 = 1,
	var2 = 0,
	nextstate = S_TVZ_SPEAR_HORIZONTAL2
}
states[S_TVZ_SPEAR_HORIZONTAL2] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|F,
	tics = 2,
	action = A_Pain,
	nextstate = S_TVZ_SPEAR_HORIZONTAL3
}
states[S_TVZ_SPEAR_HORIZONTAL3] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|G,
	tics = 2,
	nextstate = S_TVZ_SPEAR_HORIZONTAL4
}
states[S_TVZ_SPEAR_HORIZONTAL4] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|H,
	tics = -1,
	action = A_SpikeRetract,
	var1 = 0,
	var2 = 0,
	nextstate = S_TVZ_SPEAR_HORIZONTAL5
}
states[S_TVZ_SPEAR_HORIZONTAL5] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|G,
	tics = 2,
	action = A_Pain,
	nextstate = S_TVZ_SPEAR_HORIZONTAL6
}
states[S_TVZ_SPEAR_HORIZONTAL6] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|F,
	tics = 2,
	nextstate = S_TVZ_SPEAR_HORIZONTAL1
}
states[S_TVZ_SPEAR_HORIZONTAL_BASE] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|I,
	tics = -1,
	nextstate = S_NULL
}

-- behavior: vertical spear
addHook("MobjCollide", function(spike, pmo)
	if not pmo then return end
	if not pmo.player then return end
	if not spike then return end
	if not (spike.type == MT_TVZ_SPEAR_VERTICAL) then return end
	if pmo.player.playerstate != PST_LIVE then return end

	if (pmo.z > spike.z+spike.height) or (pmo.z+pmo.height < spike.z) then return end
	P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)

	/*
	if (spike.state == S_TVZ_SPEAR_VERTICAL1 and spike.tics == AngleFixed(spike.angle)/FRACUNIT-1 and not (spike.flags & MF_SCENERY))
		P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)
		if (spike.eflags & MFE_VERTICALFLIP)
			P_SetOrigin(pmo, spike.x, spike.y, spike.z-pmo.height)
		else
			P_SetOrigin(pmo, spike.x, spike.y, spike.z+spike.height)
		end
	end

	if (spike.eflags & MFE_VERTICALFLIP)
		if ((pmo.z + pmo.height <= spike.z+(spike.height/6)) and (pmo.z+pmo.height >= spike.z-(spike.height/6)))
			P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)
		end
	else
		if ((pmo.z >= spike.z+(spike.height/6)*5) and (pmo.z <= spike.z+(spike.height/6)*7))
			P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)
		end
	end
	*/
end)

addHook("MobjFuse", function(mo)
	if not mo then return end
	if not mo.valid then return end

	mo.state = states[mo.state].nextstate
	if (mo.spawnpoint) then
		mo.fuse = mo.spawnpoint.args[0]
	else
		mo.fuse = mo.info.speed
	end
	return true
end, MT_TVZ_SPEAR_VERTICAL)

addHook("MapThingSpawn", function(mo, mt)
	local TMSF_RETRACTED = 1
	local TMSF_INTANGIBLE = 1<<1

	if mt.args[0] then
		mo.flags = $ & ~MF_SCENERY
		mo.fuse = mt.args[1]
	end

	if mt.args[2] & TMSF_RETRACTED then
		mo.state = mo.info.meleestate
	end

	if not (mt.args[2] & TMSF_INTANGIBLE) and not (metalrecording) then
		mo.flags = $ & ~(MF_NOBLOCKMAP|MF_NOGRAVITY|MF_NOCLIPHEIGHT)
		mo.flags = $ | MF_SOLID
	end
end, MT_TVZ_SPEAR_VERTICAL)

-- behavior: horizontal spear
addHook("MobjCollide", function(spike, pmo)
	if not pmo then return end
	if not pmo.player then return end
	if not spike then return end
	if not (spike.type == MT_TVZ_SPEAR_HORIZONTAL) then return end
	if pmo.player.playerstate != PST_LIVE then return end
	if not spike.tracer then return end

	if (pmo.z > spike.z+spike.height) or (pmo.z+pmo.height < spike.z) then return end
	P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)
	/*
	-- did we run into the spikes?
	local touchangle = R_PointToAngle2(spike.tracer.x, spike.tracer.y, pmo.x, pmo.y)
	local bottomz
	local topz
	if (spike.eflags & MFE_VERTICALFLIP)
		bottomz = spike.z+spike.height
		topz = spike.z
	else
		bottomz = spike.z
		topz = spike.z+spike.height
	end

	if ((pmo.z + pmo.height > bottomz) and (pmo.z < topz) and (R_PointToDist2(spike.tracer.x, spike.tracer.y, pmo.x, pmo.y) > (spike.radius/16)*15))
		touchangle = spike.angle - $
		if (touchangle > ANGLE_180)
			touchangle = InvAngle(touchangle)
		end
		if (touchangle <= ANGLE_22h)
			P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)
		end
	end

	-- getting stabbed by extending spikes
	if not spike.spawnpoint then return end
	if (spike.state == S_TVZ_SPEAR_HORIZONTAL1 and spike.tics == spike.spawnpoint.args[1]-1 and not (spike.flags & MF_SCENERY))
		P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)
		P_SetOrigin(pmo, spike.x+P_ReturnThrustX(spike, spike.angle, spike.radius), spike.y+P_ReturnThrustY(spike, spike.angle, spike.radius), spike.z+spike.height/2)
	end
	*/
end)

addHook("MapThingSpawn", function(mo, mt)
	local TMSF_RETRACTED = 1
	local TMSF_INTANGIBLE = 1<<1

	if mt.args[0] then
		mo.flags = $ & ~MF_SCENERY
		mo.fuse = mt.args[1]
	end

	if mt.args[2] & TMSF_RETRACTED then
		mo.state = mo.info.meleestate
	end

	if not (mt.args[2] & TMSF_INTANGIBLE) and not (metalrecording) then
		mo.flags = $ & ~(MF_NOBLOCKMAP|MF_NOGRAVITY|MF_NOCLIPHEIGHT)
		mo.flags = $ | MF_SOLID
	end

	local base = P_SpawnMobj(mo.x - P_ReturnThrustX(mo, mo.angle - ANGLE_90, mo.radius), mo.y - P_ReturnThrustY(mo, mo.angle - ANGLE_90, mo.radius), mo.z, MT_TVZ_SPEAR_HORIZONTAL_BASE)
	base.angle = mo.angle
	base.scale = mo.scale
	base.target = mo
	mo.tracer = base
end, MT_TVZ_SPEAR_HORIZONTAL)

addHook("MobjFuse", function(mo)
	if not mo then return end
	if not mo.valid then return end

	mo.state = states[mo.state].nextstate
	if (mo.spawnpoint) then
		mo.fuse = mo.spawnpoint.args[0]
	else
		mo.fuse = mo.info.speed
	end
	return true
end, MT_TVZ_SPEAR_HORIZONTAL)

addHook("MobjThinker", function(mo)
	if not mo then return end
	if not mo.valid then return end

	if not mo.target then
		P_RemoveMobj(mo)
		return
	end

	P_SetOrigin(
		mo,
		mo.target.x - P_ReturnThrustX(mo.target, mo.target.angle, mo.target.radius),
		mo.target.y - P_ReturnThrustY(mo.target, mo.target.angle, mo.target.radius),
		mo.target.z
	)
	mo.angle = mo.target.angle + ANGLE_90
	mo.scale = mo.target.scale
end, MT_TVZ_SPEAR_HORIZONTAL_BASE)