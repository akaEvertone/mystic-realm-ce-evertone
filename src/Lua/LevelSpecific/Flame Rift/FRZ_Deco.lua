freeslot("MT_DUMMYAIZ", "MT_AIZROCK1", "MT_AIZROCK2", "MT_AIZROCK3", "S_AIZROCK", "SPR_AIZR")

states[S_AIZROCK] = {
	sprite = SPR_AIZR,
	frame = A,
}

mobjinfo[MT_DUMMYAIZ] = {
	spawnstate = S_INVISIBLE,
	spawnhealth = 1000,
	reactiontime = 8,
	deathsound = sfx_pop,
	radius = 16*FRACUNIT,
	height = 32*FRACUNIT,
	mass = 100,
	flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY
}

mobjinfo[MT_AIZROCK1] = {
--$Category Flame Rift
--$Name FRZ ROCK (Small)
--$Sprite AIZRA0
	doomednum = 2601,
	spawnstate = S_INVISIBLE,
	spawnhealth = 1000,
	reactiontime = 8,
	deathsound = sfx_pop,
	radius = 40*FRACUNIT,
	height = 50*FRACUNIT,
	mass = 100,
	flags = MF_SCENERY|MF_SOLID|MF_NOGRAVITY
}

mobjinfo[MT_AIZROCK2] = {
--$Category Flame Rift
--$Name FRZ ROCK (Mid)
--$Sprite AIZRF0
	doomednum = 2602,
	spawnstate = S_INVISIBLE,
	spawnhealth = 1000,
	reactiontime = 8,
	deathsound = sfx_pop,
	radius = 40*FRACUNIT,
	height = 78*FRACUNIT,
	mass = 100,
	flags = MF_SCENERY|MF_SOLID|MF_NOGRAVITY
}

mobjinfo[MT_AIZROCK3] = {
--$Category Flame Rift
--$Name FRZ ROCK (Big)
--$Sprite AIZRG0
	doomednum = 2603,
	spawnstate = S_INVISIBLE,
	spawnhealth = 1000,
	reactiontime = 8,
	deathsound = sfx_pop,
	radius = 40*FRACUNIT,
	height = 128*FRACUNIT,
	mass = 100,
	flags = MF_SCENERY|MF_SOLID|MF_NOGRAVITY
}

addHook("MapThingSpawn", function(a, tm)
	a.rocktable = {}

	local ang = tm.angle*ANG1
	local buttonspr = {A, B, A, B}
	for i = 1,4 do
		local angt = ang+i*ANGLE_90
		local button = P_SpawnMobjFromMobj(a, 28*cos(angt),28*sin(angt),0, MT_DUMMYAIZ)
		button.state = S_AIZROCK
		button.angle = angt
		button.frame = buttonspr[i]
		table.insert(a.rocktable, button)
	end

	local topspr = {E, D, E, D}
	for i = 1,4 do
		local angt = ang+i*ANGLE_90
		local top = P_SpawnMobjFromMobj(a, 18*cos(angt),18*sin(angt),32*FRACUNIT, MT_DUMMYAIZ)
		top.state = S_AIZROCK
		top.angle = angt
		top.frame = topspr[i]
		table.insert(a.rocktable, top)
	end

end, MT_AIZROCK1)

addHook("MapThingSpawn", function(a, tm)
	a.rocktable = {}

	local ang = tm.angle*ANG1
	local buttonspr = {A, B, A, B}
	for i = 1,4 do
		local angt = ang+i*ANGLE_90
		local button = P_SpawnMobjFromMobj(a, 25*cos(angt),25*sin(angt),0, MT_DUMMYAIZ)
		button.state = S_AIZROCK
		button.frame = buttonspr[i]
		button.angle = angt
		table.insert(a.rocktable, button)
	end

	for i = 1,2 do
		local angt = ang+i*ANGLE_180+ANGLE_45
		local top = P_SpawnMobjFromMobj(a, 12*cos(angt),12*sin(angt),30*FRACUNIT, MT_DUMMYAIZ)
		top.state = S_AIZROCK
		top.frame = I
		top.angle = angt
		table.insert(a.rocktable, top)
	end

	local topspr = {F, C, F, C}
	for i = 1,4 do
		local angt = ang+i*ANGLE_90
		local top = P_SpawnMobjFromMobj(a, 22*cos(angt),22*sin(angt),42*FRACUNIT, MT_DUMMYAIZ)
		top.state = S_AIZROCK
		top.frame = topspr[i]
		top.angle = angt
		table.insert(a.rocktable, top)
	end

end, MT_AIZROCK2)

addHook("MapThingSpawn", function(a, tm)
	a.rocktable = {}

	local ang = tm.angle*ANG1
	local buttonspr = {H, G, H, G}
	for i = 1,4 do
		local angt = ang+i*ANGLE_90
		local button = P_SpawnMobjFromMobj(a, 25*cos(angt),25*sin(angt),0, MT_DUMMYAIZ)
		button.state = S_AIZROCK
		button.frame = buttonspr[i]
		button.angle = angt
		table.insert(a.rocktable, button)
	end

	for i = 1,2 do
		local angt = ang+i*ANGLE_180+ANGLE_45
		local top = P_SpawnMobjFromMobj(a, 12*cos(angt),12*sin(angt),70*FRACUNIT, MT_DUMMYAIZ)
		top.state = S_AIZROCK
		top.frame = I
		top.angle = angt
		table.insert(a.rocktable, top)
	end

	local topspr = {F, C, F, C}
	for i = 1,4 do
		local angt = ang+i*ANGLE_90
		local top = P_SpawnMobjFromMobj(a, 22*cos(angt),22*sin(angt),82*FRACUNIT, MT_DUMMYAIZ)
		top.state = S_AIZROCK
		top.frame = topspr[i]
		top.angle = angt
		table.insert(a.rocktable, top)
	end

end, MT_AIZROCK3)

for _,rocc in pairs({
	MT_AIZROCK1,
	MT_AIZROCK2,
	MT_AIZROCK3
	}) do

addHook("MobjDeath", function(a, tm)
	S_StartSound(tm, sfx_s3k59)
	for num, rock in ipairs(a.rocktable) do
		rock.momx = tm.momx/8+4*cos(rock.angle)
		rock.momy =	tm.momy/8+4*sin(rock.angle)
		rock.momz = 8*FRACUNIT
		rock.flags = $ &~ MF_NOGRAVITY
		rock.fuse = 2*TICRATE
		rock.rotate = true
	end
end, rocc)

addHook("MobjCollide", function(a, tm)
	if tm.z <= (a.z + a.height + 4 << FRACBITS) and a and a.valid then
		if tm.type == MT_PLAYER and (tm.player.pflags & PF_SPINNING) then
			P_KillMobj(a, tm)
		elseif a.type == MT_AIZROCK1 and tm.player.pflags & PF_JUMPED and (tm.z >=( a.z + a.height - 5 << FRACBITS)) then
			tm.momz = 10 << FRACBITS
			P_KillMobj(a, tm)
		end
	end
end, rocc)

end

addHook("MobjThinker", function(a)
	if a.rotate == true then
		a.rollangle = $ + ANG2
	end
end, MT_DUMMYAIZ)