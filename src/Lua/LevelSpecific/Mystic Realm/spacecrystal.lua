freeslot(
	'MT_SPACECRYSTAL',
	'MT_SPACECRYSTALDUMMY',
	'S_SPACECRYSTAL',
	'SPR_MR0C')

local COLOR_LUT = {
SKINCOLOR_WHITE,
SKINCOLOR_BONE,
SKINCOLOR_CLOUDY,
SKINCOLOR_GREY,
SKINCOLOR_SILVER,
SKINCOLOR_CARBON,
SKINCOLOR_JET,
SKINCOLOR_BLACK,
SKINCOLOR_AETHER,
SKINCOLOR_SLATE,
SKINCOLOR_BLUEBELL,
SKINCOLOR_PINK,
SKINCOLOR_YOGURT,
SKINCOLOR_BROWN,
SKINCOLOR_BRONZE,
SKINCOLOR_TAN,
SKINCOLOR_BEIGE,
SKINCOLOR_MOSS,
SKINCOLOR_AZURE,
SKINCOLOR_LAVENDER,
SKINCOLOR_RUBY,
SKINCOLOR_SALMON,
SKINCOLOR_RED,
SKINCOLOR_CRIMSON,
SKINCOLOR_FLAME,
SKINCOLOR_KETCHUP,
SKINCOLOR_PEACHY,
SKINCOLOR_QUAIL,
SKINCOLOR_SUNSET,
SKINCOLOR_COPPER,
SKINCOLOR_APRICOT,
SKINCOLOR_ORANGE,
SKINCOLOR_RUST,
SKINCOLOR_GOLD,
SKINCOLOR_SANDY,
SKINCOLOR_YELLOW,
SKINCOLOR_OLIVE,
SKINCOLOR_LIME,
SKINCOLOR_PERIDOT,
SKINCOLOR_APPLE,
SKINCOLOR_GREEN,
SKINCOLOR_FOREST,
SKINCOLOR_EMERALD,
SKINCOLOR_MINT,
SKINCOLOR_SEAFOAM,
SKINCOLOR_AQUA,
SKINCOLOR_TEAL,
SKINCOLOR_WAVE,
SKINCOLOR_CYAN,
SKINCOLOR_SKY,
SKINCOLOR_CERULEAN,
SKINCOLOR_ICY,
SKINCOLOR_SAPPHIRE,
SKINCOLOR_CORNFLOWER,
SKINCOLOR_BLUE,
SKINCOLOR_COBALT,
SKINCOLOR_VAPOR,
SKINCOLOR_DUSK,
SKINCOLOR_PASTEL,
SKINCOLOR_PURPLE,
SKINCOLOR_BUBBLEGUM,
SKINCOLOR_MAGENTA,
SKINCOLOR_NEON,
SKINCOLOR_VIOLET,
SKINCOLOR_LILAC,
SKINCOLOR_PLUM,
SKINCOLOR_RASPBERRY,
SKINCOLOR_ROSY
}

-- Yes, it is MT_LANTERN but special by being smaller script wise. :P

addHook("MapThingSpawn", function(mo, mt)
	local color_arg = mt.stringargs[0] and
	string.upper(mt.stringargs[0] or '') or nil					-- Color
	local sine_arg = mt.args[0] 								-- Z Range
	local spee_arg = mt.args[1]									-- Z Speed
	local rota_arg = mt.args[2]									-- Rotate Speed

	if color_arg and _G[color_arg] and skincolors[_G[color_arg]]
	and skincolors[_G[color_arg]] ~= SKINCOLOR_NONE then
		mo.extravalue1 = _G[color_arg]
	else
		mo.extravalue1 = SKINCOLOR_APPLE
		if color_arg then
			print("\x85".."WARNING:".."\x80".."Object #"..#mt.." has invalid skincolor indentificator!")
		end
	end

	mo.extravalue2 = sine_arg >> 2
	mo.speed = spee_arg*(ANG1 >> 2)
	mo.cusval = rota_arg*ANG1
end, MT_SPACECRYSTAL)

addHook("MobjSpawn", function(mo)
	local side = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_SPACECRYSTALDUMMY)
	side.frame = mo.frame
	side.color = mo.color
	mo.target = side
end, MT_SPACECRYSTAL)

addHook("MobjThinker", function(mo)
	if mo.extravalue2 then
		mo.z = mo.z+(mo.extravalue2*sin(mo.threshold))*P_MobjFlip(mo)
		if mo.hasbeencolored then
			mo.threshold = $+mo.speed
		end
	end

	if mo.target then
		mo.target.z = mo.z
		mo.target.color = mo.color
		mo.target.angle = mo.angle+ANGLE_90
		mo.target.scale = mo.scale
		mo.target.frame = mo.frame
	else
		local side = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_SPACECRYSTALDUMMY)
		side.color = mo.color
		mo.target = side
		mo.target.scale = mo.scale
		mo.target.frame = mo.frame
	end

	mo.angle = mo.angle+mo.cusval

	if mo.hasbeencolored then return false end

	mo.color = mo.extravalue1 or 0

	if udmf and not mo.extravalue2 and mo.spawnpoint and mo.spawnpoint.valid then
		local sine_arg = mo.spawnpoint.args[0] 				-- Z Range
		local spee_arg = mo.spawnpoint.args[1] 				-- Z Speed

		mo.extravalue2 = sine_arg >> 2
		mo.speed = spee_arg*(ANG1 >> 2)
	end

	mo.hasbeencolored = true
end, MT_SPACECRYSTAL)

mobjinfo[MT_SPACECRYSTAL] = {
--$NotAngled
--$Category Mystic Realm
--$Name Realm Crystal
--$Sprite MR0CA0
--$Arg0 Z Movement Range
--$Arg1 Z Movement Speed
--$Arg2 Rotatory Speed
        doomednum = 1480,
        spawnstate = S_SPACECRYSTAL,
        spawnhealth = 1,
        radius = 22*FRACUNIT,
        height = 47*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT
}

mobjinfo[MT_SPACECRYSTALDUMMY] = {
        spawnstate = S_SPACECRYSTAL,
        spawnhealth = 1,
        radius = 22*FRACUNIT,
        height = 47*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT
}

states[S_SPACECRYSTAL] = {
	sprite = SPR_MR0C,
	frame = A|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}
