states[S_BROKENCRAWLA_STND] = {
	sprite = SPR_BCRW,
	frame = A,
	tics = 5,
	action = A_Look,
	var1 = 0,
	var2 = 0,
	nextstate = S_BROKENCRAWLA_STND
}

states[S_BROKENCRAWLA_ATK1] = {
	sprite = SPR_BCRW,
	frame = A,
	tics = 30,
	action = None,
	var1 = 0,
	var2 = 0,
	nextstate = S_BROKENCRAWLA_ATK3
}

states[S_BROKENCRAWLA_ATK2] = {
	sprite = SPR_BCRW,
	frame = A,
	tics = 40,
	action = A_DualAction,
	var1 = S_BROKENCRAWLA_ATK3,
	var2 = S_BROKENCRAWLA_ATKE,
	nextstate = S_BROKENCRAWLA_ATK1
}

states[S_BROKENCRAWLA_ATK3] = {
	sprite = SPR_BCRW,
	frame = B,
	tics = 40,
	action = A_MultiShot,
	var1 = MT_SHOCKWAVE*65536+8,
	var2 = -45,
	nextstate = S_BROKENCRAWLA_ATK1
}

states[S_BROKENCRAWLA_ATKE] = {
	sprite = SPR_BCRW,
	frame = A,
	tics = 40,
	action = A_PlaySound,
	var1 = 401,
	var2 = 1,
	nextstate = S_NULL
}

mobjinfo[MT_BROKENCRAWLA] = {
	--$Name DerelictCrawla Commander
	--$Category Sunken Plant
	--$Sprite BCRWA1
	doomednum = 3100,
	spawnstate = S_BROKENCRAWLA_STND,
	spawnhealth = 1,
	seestate = S_BROKENCRAWLA_ATK1,
	seesound = sfx_None,
	reactiontime = 35,
	attacksound = sfx_None,
	activesound = sfx_None,
	painstate = S_NULL,
	painchance = 200,
	painsound = sfx_None,
	meleestate = S_NULL,
	missilestate = S_NULL,
	deathstate = S_XPLD1,
	xdeathstate = S_NULL,
	deathsound = sfx_pop,
	raisestate = S_NULL,
	speed = 0,
	radius = 24*FRACUNIT,
	height = 32*FRACUNIT,
	dispoffset = 0,
	mass = 100,
	damage = 0,
	flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE
}

addHook("MobjSpawn", function(mo)
	mo.rollangle = P_RandomRange(1,360)*ANG1
    mo.samusHP = 4 --1 samusHP = 1 PowerBeam shot. Missiles and charge shots bypass SamusHP and take regular HP.
    mo.spazresist = true
	mo.plasmaresist = true
	mo.waveweak = true
end, MT_BROKENCRAWLA)

local function ContinuousLaser(mo, objectType, positionType)
	local x, y, z

	local angle
	local point
	local duration
	local LASERCOLORS =
	{
		SKINCOLOR_LIME
	}

	if not mo.target then
		mo.extravalue2 = 0
		return
	end

	if (mo.extravalue2 > 0) then
		mo.extravalue2 = $ - 1
	end

	duration = mo.extravalue2

	if (positionType == 0) then
		x = mo.x + P_ReturnThrustX(mo, mo.angle+ANGLE_90, FixedMul(44*FRACUNIT, mo.scale))
		y = mo.y + P_ReturnThrustY(mo, mo.angle+ANGLE_90, FixedMul(44*FRACUNIT, mo.scale))
		if (mo.eflags & MFE_VERTICALFLIP) then
			z = mo.z + mo.height - FixedMul(56*FRACUNIT, mo.scale) - mobjinfo[objectType].height
		else
			z = mo.z + FixedMul(56*FRACUNIT, mo.scale)
		end
	elseif (positionType == 1) then
		x = mo.x + P_ReturnThrustX(mo, mo.angle-ANGLE_90, FixedMul(44*FRACUNIT, mo.scale))
		y = mo.y + P_ReturnThrustY(mo, mo.angle-ANGLE_90, FixedMul(44*FRACUNIT, mo.scale))
		if (mo.eflags & MFE_VERTICALFLIP) then
			z = mo.z + mo.height - FixedMul(56*FRACUNIT, mo.scale) - mobjinfo[objectType].height
		else
			z = mo.z + FixedMul(56*FRACUNIT, mo.scale)
		end
	elseif (positionType == 2) then
		--Fire THREE lasers
		ContinuousLaser(mo, objectType, 3)
		ContinuousLaser(mo, objectType, 0)
		ContinuousLaser(mo, objectType, 1)
		return
	elseif (positionType == 3) then
		x = mo.x + P_ReturnThrustX(mo, mo.angle, FixedMul(42*FRACUNIT, mo.scale))
		y = mo.y + P_ReturnThrustY(mo, mo.angle, FixedMul(42*FRACUNIT, mo.scale))
		z = mo.z + mo.height/2
	else
		x = mo.x
		y = mo.y
		z = mo.z + mo.height/2
	end


	if ((not (mo.flags2 & MF2_FIRING)) and duration > 1) then

		mo.angle = R_PointToAngle2(x, y, mo.target.x, mo.target.y)
		if (mobjinfo[objectType].seesound) then
			S_StartSound(mo, mobjinfo[objectType].seesound)
		end

		point = P_SpawnMobj(
				x + P_ReturnThrustX(mo, mo.angle, mo.radius),
				y + P_ReturnThrustY(mo, mo.angle, mo.radius),
				mo.z - mo.height / 2, MT_EGGMOBILE_TARGET
			)
		if (point and point.valid) then
			point.angle = mo.angle
			point.fuse = duration+1
			point.target = mo.target
			mo.target = point
		end
	end

	angle = R_PointToAngle2(z + (mobjinfo[objectType].height>>1), 0, mo.target.z, R_PointToDist2(x, y, mo.target.x, mo.target.y))

	point = P_SpawnMobj(x, y, z, objectType)
	if (not (point and point.valid)) then
		return
	end

	point.target = mo
	point.angle = mo.angle
	local speed = point.radius
	point.momz = FixedMul(cos(angle), speed)
	point.momx = FixedMul(sin(angle), FixedMul(cos(point.angle), speed))
	point.momy = FixedMul(sin(angle), FixedMul(sin(point.angle), speed))
	point.fuse = duration+1

	local storedmomx = point.momx
	local storedmomy = point.momy

	for i = 0, 255 do
		local spawnedmo = P_SpawnMobj(point.x, point.y, point.z, point.type)
		if (spawnedmo and spawnedmo.valid) then
			spawnedmo.angle = point.angle
			spawnedmo.color = LASERCOLORS[1] --laser cycle

			spawnedmo.flags = MF_NOCLIP|MF_NOCLIPHEIGHT|MF_NOGRAVITY|MF_SCENERY

			if ((duration & 1) and spawnedmo.info.missilestate) then

				spawnedmo.state = spawnedmo.info.missilestate
				if (spawnedmo.info.meleestate) then
					local mo2 = P_SpawnMobjFromMobj(spawnedmo, 0, 0, 0, MT_PARTICLE)
					if (mo2 and mo2.valid) then
						mo2.flags2 =  $ | MF2_LINKDRAW
						mo2.tracer = spawnedmo
						mo2.state = spawnedmo.info.meleestate
					end
				end
			end

			if (duration == 1) then
				P_SpawnGhostMobj(spawnedmo)
			end

			x = point.x
			y = point.y
			z = point.z
			if (P_RailThinker(point)) then
				break
			end
		end
	end

	x = $ + storedmomx
	y = $ + storedmomy

	local floorz = P_FloorzAtPos(x, y, z, mobjinfo[MT_EGGMOBILE_FIRE].height)
	if (z - floorz < mobjinfo[MT_EGGMOBILE_FIRE].height>>1 and duration & 1) then

		point = P_SpawnMobj(x, y, floorz, MT_EGGMOBILE_FIRE)
		if (point and point.valid) then

			point.angle = mo.angle
			point.destscale = mo.scale
			P_SetScale(point, point.destscale)
			point.target = mo
			if (point.eflags & (MFE_UNDERWATER|MFE_TOUCHWATER)) then
				for i = 0, 1 do
					local size = 3
					local steam = P_SpawnMobj(x, y, point.watertop - size*mobjinfo[MT_DUST].height, MT_DUST)
					if (steam and steam.valid) then
						P_SetScale(steam, size*mo.scale)
						P_SetObjectMomZ(steam, FRACUNIT + 2*P_RandomFixed(), true)
						P_InstaThrust(steam, FixedAngle(P_RandomKey(360)*FRACUNIT), 2*P_RandomFixed())
						if (point.info.painsound) then
							S_StartSound(steam, point.info.painsound)
						end
					end
				end
			else
				local distx = P_ReturnThrustX(point, point.angle, point.radius)
				local disty = P_ReturnThrustY(point, point.angle, point.radius)
				if (P_TryMove(point, point.x + distx, point.y + disty, false) -- prevents the sprite from clipping into the wall or dangling off ledges
					and P_TryMove(point, point.x - 2*distx, point.y - 2*disty, false)
					and P_TryMove(point, point.x + distx, point.y + disty, false))
				then
					if (point.info.seesound) then
						S_StartSound(point, point.info.seesound)
					end
				else
					P_RemoveMobj(point)
				end
			end
		end
	end

	if (duration > 1) then
		mo.flags2 =  $ | MF2_FIRING
	else
		mo.flags2 = $ & ~MF2_FIRING
	end
end

local playerdist = 6 --adjust this as needed to change base search distance when looking for a nearby player before shooting

addHook("MobjThinker", function(mo)
	if mo.state == S_LASERDUDE_LASER3 and mo.target == nil then
		A_FindTarget(mo, MT_LASERTARGET, 0)
	end

	mo.lasertime = $ or false

	if mo.state == S_LASERDUDE_LASER4 and mo.target ~= nil then --ready to shoot and have a target orb?
		for p in players.iterate do --lets look for players first
			if not (multiplayer and p.playerstate ~= PST_LIVE) 
			and not (R_PointToDist2(mo.x, mo.y, p.mo.x, p.mo.y) > RING_DIST * playerdist) then --if we're in mp and our prospected player is ded
				--are we close enough? if not then stop it
				--A_Boss1Laser(mo, MT_LASER, (0*65536)+500) --still there? then shoot
				mo.extravalue2 = 60
				mo.lasertime = true
				return --end the search so we don't create multiple lasers per player
			end
		end
	end
	if mo.lasertime then
		if mo.extravalue2 > 0 and mo.target then
			ContinuousLaser(mo, MT_LASER, 4)
		else
			mo.lasertime = false
			mo.extravalue2 = 0
		end
	end
	--print(mo.target and mo.target.x or "searching")
end, MT_LASERDUDE)

addHook("MobjSpawn", function(mo)
	if mapheaderinfo[gamemap].lvlttl == "Sunken Plant"
	or mapheaderinfo[gamemap].lvlttl == "Nitric Citadel" then
		mo.colorized = true
		mo.color = SKINCOLOR_LIME
	end
end, MT_EGGMOBILE_FIRE)
