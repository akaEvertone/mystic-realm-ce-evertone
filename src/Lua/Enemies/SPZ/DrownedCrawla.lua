freeslot(
	"MT_DWCRAWL",
	"S_DWCRAWL_STND",
	"S_DWCRAWL_RUN1",
	"S_DWCRAWL_RUN2",
	"S_DWCRAWL_RUN3",
	"S_DWCRAWL_RUN4",
	"S_DWCRAWL_RUN5",
	"S_DWCRAWL_RUN6"
)



mobjinfo[MT_DWCRAWL] = {
	--$Name Drowned Crawla
	--$Category Sunken Plant
	--$NotAngled
	--$Sprite POSSA1
	doomednum = 3105,
	spawnstate = S_DWCRAWL_STND,
	spawnhealth = 1,
	speed = 1,
	radius = 24*FRACUNIT,
	height = 32*FRACUNIT,
	attacksound = sfx_none,
	activesound = sfx_None,
	painstate = S_NULL,
	painchance = 200,
	painsound = sfx_None,
	meleestate = S_NULL,
	missilestate = S_NULL,
	deathstate = S_NULL,
	xdeathstate = S_NULL,
	deathsound = sfx_pop,
	raisestate = S_NULL,
	reactiontime = 35,
	seestate = S_DWCRAWL_RUN1,
	seesound = sfx_None,
	mass = 100,
	dispoffset = 0,
	flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE
}



states[S_DWCRAWL_STND] = {
	sprite = POSSA1,
	tics = 5,
	Action = A_Look,
	Var1 = 0,
	Var2 = 0,
	nextstate = S_DWCRAWL_STND
}



states[S_DWCRAWL_RUN1] = {
	sprite = POSSA1,
	tics = 5,
	nextstate = S_DWCRAWL_RUN2,
	Action = A_Chase,
	Var1 = 0,
	Var2 = 0
}

states[S_DWCRAWL_RUN2] = {
	sprite = POSSA1,
	tics = 5,
	nextstate = S_DWCRAWL_RUN3,
	Action = A_Chase,
	Var1 = 0,
	Var2 = 0
}

states[S_DWCRAWL_RUN3] = {
	sprite = POSSA1,
	tics = 5,
	nextstate = S_DWCRAWL_RUN4,
	Action = A_Chase,
	Var1 = 0,
	Var2 = 0
}

states[S_DWCRAWL_RUN4] = {
	sprite = POSSA1,
	tics = 5,
	nextstate = S_DWCRAWL_RUN5,
	Action = A_Chase,
	Var1 = 0,
	Var2 = 0
}

states[S_DWCRAWL_RUN5] = {
	sprite = POSSA1,
	tics = 5,
	nextstate = S_DWCRAWL_RUN6,
	Action = A_Chase,
	Var1 = 0,
	Var2 = 0
}

states[S_DWCRAWL_RUN6] = {
	sprite = POSSA1,
	tics = 5,
	nextstate = S_DWCRAWL_RUN1,
	Action = A_Chase,
	Var1 = 0,
	Var2 = 0
}