freeslot(
	"S_LASERDUDE_LASER1",
	"S_LASERDUDE_LASER2",
	"S_LASERDUDE_LASER4",
	"S_LASERTARGET_STAND",
	"S_LASERTARGET_CHASE1",
	"S_LASERTARGET_CHASE2",
	"S_LASERTARGET_CHASE3",
	"S_LASERTARGET_CHASE4",
	"S_LASERTARGET_CHASE5",
	"S_LASERTARGET_CHASE6",
	"S_LASERTARGET_CHASE7",
	"S_LASERTARGET_RESET1",
	"S_LASERTARGET_RESET2",
	"S_LASERTARGET_PAIN",
	"SPR_BNCR",
	"SPR_BMER"
)


mobjinfo[MT_LASERTARGET] = {
	--$Name Bouncer
	--$Category Sunken Plant
	--$Sprite BNCRA1
	doomednum = 3006,
	spawnstate = S_LASERTARGET_STAND,
	spawnhealth = 1000,
	speed = 10,
	radius = 16*FRACUNIT,
	height = 16*FRACUNIT,
	attacksound = sfx_none,
	activesound = sfx_s1b4,
	painstate = S_LASERTARGET_PAIN,
	painchance = -1,
	painsound = sfx_s3k7b,
	meleestate = S_NULL,
	missilestate = S_NULL,
	deathstate = S_XPLD1,
	xdeathstate = S_NULL,
	deathsound = sfx_pop,
	raisestate = S_LASERTARGET_PAIN,
	reactiontime = 5,
	seestate = S_LASERTARGET_CHASE1,
	seesound = sfx_None,
	mass = 8*FRACUNIT,
	dispoffset = 0,
	flags = MF_ENEMY|MF_SHOOTABLE|MF_BOUNCE|MF_SPRING
}


states[S_LASERTARGET_PAIN] = {
sprite = SPR_BNCR,
frame = A,
action = A_DoNPCPain,
var1 = FRACUNIT,
tics = 1,
nextstate = S_LASERTARGET_STAND
}

states[S_LASERTARGET_STAND] = {
sprite = SPR_BNCR,
frame = A,
action = A_Look,
tics = 1,
nextstate = S_LASERTARGET_STAND
}

states[S_LASERTARGET_CHASE1] = {
sprite = SPR_BNCR,
frame = A,
action = A_Chase,
tics = 4,
nextstate = S_LASERTARGET_CHASE2
}

states[S_LASERTARGET_CHASE2] = {
sprite = SPR_BNCR,
frame = A,
action = A_Chase,
tics = 3,
nextstate = S_LASERTARGET_CHASE3
}

states[S_LASERTARGET_CHASE3] = {
sprite = SPR_BNCR,
frame = A,
action = A_CheckRange,
var1 = 128+128+128+128<<0,
var2 = S_LASERTARGET_CHASE4,
tics = 1,
nextstate = S_LASERTARGET_CHASE1
}

states[S_LASERTARGET_CHASE4] = {
sprite = SPR_BNCR,
frame = C,
action = A_FaceTarget,
var1 = 8,
var2 = 15,
tics = 1,
nextstate = S_LASERTARGET_CHASE5
}

states[S_LASERTARGET_CHASE5] = {
sprite = SPR_BNCR,
frame = B,
action = A_BunnyHop,
var1 = 8,
var2 = 15,
tics = 2,
nextstate = S_LASERTARGET_CHASE6
}

states[S_LASERTARGET_CHASE6] = {
sprite = SPR_BNCR,
frame = A,
action = A_Boss5CheckOnGround,
var1 = S_LASERTARGET_CHASE1,
tics = 1,
nextstate = S_LASERTARGET_CHASE7
}

states[S_LASERTARGET_CHASE7] = {
sprite = SPR_BNCR,
frame = C,
action = A_Repeat,
var1 = 70,
var2 = S_LASERTARGET_CHASE6,
tics = 1,
nextstate = S_LASERTARGET_RESET1
}

states[S_LASERTARGET_RESET1] = {
sprite = SPR_BNCR,
frame = A,
action = A_SetObjectFlags,
var1 = MF_STICKY,
var2 = 1,
tics = 35,
nextstate = S_LASERTARGET_RESET2
}

states[S_LASERTARGET_RESET2] = {
sprite = SPR_BNCR,
frame = A,
action = A_SetObjectFlags,
var1 = MF_STICKY,
var2 = 2,
tics = 1,
nextstate = S_LASERTARGET_CHASE1
}

mobjinfo[MT_LASERDUDE] = {
	--$Name Beamer
	--$Category Sunken Plant
	--$Sprite BMERA1
	doomednum = 3005,
	spawnstate = S_LASERDUDE_LASER1,
	spawnhealth = 1,
	speed = 3,
	radius = 24*FRACUNIT,
	height = 32*FRACUNIT,
	attacksound = sfx_s3k53,
	activesound = sfx_s3k7b,
	painstate = S_NULL,
	painchance = -1,
	painsound = sfx_None,
	meleestate = S_NULL,
	missilestate = S_NULL,
	deathstate = S_XPLD1,
	xdeathstate = S_NULL,
	deathsound = sfx_pop,
	raisestate = S_NULL,
	reactiontime = 5,
	seestate = S_LASERDUDE_LASER2,
	seesound = sfx_None,
	mass = 8*FRACUNIT,
	dispoffset = 0,
	flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE|MF_NOGRAVITY
}


states[S_LASERDUDE_LASER1] = {
sprite = SPR_BMER,
frame = A,
action = A_BossJetFume,
var1 = 4,
tics = 1,
nextstate = S_LASERDUDE_LASER2
}

states[S_LASERDUDE_LASER2] = {
sprite = SPR_BMER,
frame = B,
action = A_PlaySound,
var1 = sfx_s3k53,
var2 = 1,
tics = 35,
nextstate = S_LASERDUDE_LASER3
}

states[S_LASERDUDE_LASER3] = {
sprite = SPR_BMER,
frame = C,
action = none,
tics = 45,
nextstate = S_LASERDUDE_LASER4
}

states[S_LASERDUDE_LASER4] = {
sprite = SPR_BMER,
frame = C,
action = none,
tics = 55,
nextstate = S_LASERDUDE_LASER1
}


