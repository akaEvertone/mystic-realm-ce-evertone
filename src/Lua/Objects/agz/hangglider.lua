--[[
    Aerial Garden Hangglider

    This object is composed of 3 components
    - the main object (handles)
    - 2 panels acting as the visual glider portion

    (C) 2022-2023 by K. "ashifolfi" J.
]]

freeslot(
    "MT_HANGGLIDER",
    "S_HANGGLIDER",
    "S_HANGGLIDER_PANEL",

    "SPR_HGPN"
)

mobjinfo[MT_HANGGLIDER] = {
    --$Name HangGlider
    --$Category Aerial Garden Zone
    --$Sprite UNKNA0
    doomednum   = 1339,
    spawnstate  = S_HANGGLIDER,
    seestate    = S_HANGGLIDER,
    spawnhealth = 8,
	radius = 20*FRACUNIT,
	height = 48*FRACUNIT,
    flags       = MF_NOGRAVITY
}

states[S_HANGGLIDER]        = {SPR_UNKN, A|FF_PAPERSPRITE, -1, nil, 0, 0, S_NULL}
states[S_HANGGLIDER_PANEL]  = {SPR_HGPN, A, -1, nil, 0, 0, S_NULL}

local COLOR_LUT = {
SKINCOLOR_CARBON,
SKINCOLOR_JET,
SKINCOLOR_BLACK,
SKINCOLOR_BLUEBELL,
SKINCOLOR_PINK,
SKINCOLOR_YOGURT,
SKINCOLOR_BROWN,
SKINCOLOR_BRONZE,
SKINCOLOR_TAN,
SKINCOLOR_BEIGE,
SKINCOLOR_MOSS,
SKINCOLOR_AZURE,
SKINCOLOR_LAVENDER,
SKINCOLOR_RUBY,
SKINCOLOR_SALMON,
SKINCOLOR_RED,
SKINCOLOR_CRIMSON,
SKINCOLOR_FLAME,
SKINCOLOR_KETCHUP,
SKINCOLOR_PEACHY,
SKINCOLOR_QUAIL,
SKINCOLOR_SUNSET,
SKINCOLOR_COPPER,
SKINCOLOR_APRICOT,
SKINCOLOR_ORANGE,
SKINCOLOR_RUST,
SKINCOLOR_GOLD,
SKINCOLOR_SANDY,
SKINCOLOR_YELLOW,
SKINCOLOR_OLIVE,
SKINCOLOR_LIME,
SKINCOLOR_PERIDOT,
SKINCOLOR_APPLE,
SKINCOLOR_GREEN,
SKINCOLOR_FOREST,
SKINCOLOR_EMERALD,
SKINCOLOR_MINT,
SKINCOLOR_SEAFOAM,
SKINCOLOR_AQUA,
SKINCOLOR_TEAL,
SKINCOLOR_WAVE,
SKINCOLOR_CYAN,
SKINCOLOR_SKY,
SKINCOLOR_CERULEAN,
SKINCOLOR_ICY,
SKINCOLOR_SAPPHIRE,
SKINCOLOR_CORNFLOWER,
SKINCOLOR_BLUE,
SKINCOLOR_COBALT,
SKINCOLOR_VAPOR,
SKINCOLOR_DUSK,
SKINCOLOR_PASTEL,
SKINCOLOR_PURPLE,
SKINCOLOR_BUBBLEGUM,
SKINCOLOR_MAGENTA,
SKINCOLOR_NEON,
SKINCOLOR_VIOLET,
SKINCOLOR_LILAC,
SKINCOLOR_PLUM,
SKINCOLOR_RASPBERRY,
SKINCOLOR_ROSY
}

addHook("MobjSpawn", function(mo)
    mo.panels = {}
	local wingcolor = COLOR_LUT[P_RandomRange(1, #COLOR_LUT)]

    for i=1,2 do
        local panel = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_THOK)
        panel.angle = mo.angle + ANGLE_90 + i == 2 and ANGLE_180 or 0
        panel.tics = -1
        panel.sprite = SPR_HGPN
        panel.frame = i - 1
        panel.flags2 = $ | (MF2_SPLAT|MF2_LINKDRAW)
        panel.renderflags = $ | (RF_FLOORSPRITE|RF_SLOPESPLAT|RF_NOSPLATBILLBOARD|RF_NOSPLATROLLANGLE)
        panel.tracer = mo
		panel.color = wingcolor

        P_CreateFloorSpriteSlope(panel)

        panel.floorspriteslope.o = {x = panel.x, y = panel.y, z = panel.z}
        panel.xydirection = mo.angle
        panel.floorspriteslope.zangle = ANGLE_45

        table.insert(mo.panels, panel)
    end
end, MT_HANGGLIDER)

addHook("MobjThinker", function(mo)

	if not (mo and mo.valid) then return end

	mo.boostv = $ or 0
	mo.boosth = $ or 0
	mo.boostp = $ or 0
	mo.letgo = $ or -1
	mo.glidetime = $ or 0

	if FixedHypot(mo.momx, mo.momy) > 0 then
		mo.boostp = FixedHypot(mo.momx, mo.momy)
	end

	if mo.letgo > 0 then
		mo.letgo = max(0, $ - 1)
		mo.momz = $-FixedMul(mo.scale, FRACUNIT/4)
	end

    for _,panel in ipairs(mo.panels) do
        panel.angle = mo.angle + ANGLE_90 - (i == 2 and ANGLE_180 or 0)
		P_MoveOrigin(panel, mo.x, mo.y, mo.z)

        panel.floorspriteslope.o = {x = panel.x, y = panel.y, z = panel.z}
        panel.floorspriteslope.xydirection = mo.angle
    end

	if not mo.target then
		mo.glidetime = 0
		return
	end

	P_InstaThrust(mo, mo.angle, mo.boosth)
	P_MoveOrigin(mo.target, mo.x, mo.y, max(mo.z - ((2 *mo.target.height) / 3), mo.floorz))
	mo.glidetime = $ + 1

end, MT_HANGGLIDER)

-- 2.0 Brak Eggman Missile, code borrowed from Retro Bosses by MIDIMan
--TODO: make the glider function more like a glider rather than a propelled missile
	--make glider obey gravity, maybe build speed when diving? if going fast enough allow to pull up
	--allow maintaining momentum when jumping off
	--note: not sure if this works at all yet, ran out of time to test anything, will continue later

-- Controllable missile code ported from p_user.c in 2.0.7
addHook("PlayerThink", function(player)
	if not (player and player.valid
	and player.mo and player.mo.valid) then
		return
	end

	if player.powers[pw_carry] == CR_GENERIC then
		local item = player.mo.tracer

		-- Controllable missile
		if item and item.valid then
			if item.type == MT_HANGGLIDER then
				-- Don't let 2-player bots control missiles
				if not (player.bot == BOT_2PAI or player.bot == BOT_2PHUMAN) then
					if item.glidetime > 20 then
						if player.cmd.forwardmove > 0 then
							item.momz = $-FixedMul(item.scale, FRACUNIT/4) --swap these and implement gravity mechanic
							item.boosth = $-FixedMul(item.scale, FRACUNIT/4) --swap these and implement gravity mechanic
							item.boostv = -min(item.momz, 0)
						elseif player.cmd.forwardmove < 0 and item.boosth > 40 * FRACUNIT then
							item.momz = $+FixedMul(item.scale, FRACUNIT/4)
							item.boosth = min(30*FRACUNIT, $ - (FRACUNIT / 2)) + (item.boostv / 3)
							item.boostv = $ - ($ / 3)
						end

						-- Fix for Simple controls
						if (player.pflags & PF_ANALOGMODE) then
							player.mo.angle = player.cmd.angleturn<<16
							player.mo.angle = $-FixedAngle(player.cmd.sidemove*FRACUNIT/10)
						end
					end

					item.angle = player.mo.angle
					P_InstaThrust(item, item.angle, FixedMul(item.info.speed, item.scale))
				end

				if P_IsObjectOnGround(player.mo) or item.health <= 0 then
					player.powers[pw_carry] = CR_NONE
					item.letgo = 20
					item.target = nil
					player.mo.tracer = nil
				end
				if player.mrce.jump > 0 and player.mo.tracer then
					player.powers[pw_carry] = CR_NONE
					item.letgo = 20
					item.target = nil
					P_InstaThrust(player.mo, item.angle, item.boostp)
					P_DoJump(player, true)
					player.mo.tracer = nil
				end
			end
		else
			player.powers[pw_carry] = CR_NONE
		end
	end
end)

addHook("MobjMoveCollide", function(tmthing, thing)
	if not (tmthing and tmthing.valid
	and thing and thing.valid) then
		return
	end

	if tmthing.z <= thing.z + thing.height
	and thing.z <= tmthing.z + tmthing.height then
		if tmthing.letgo > 0 then return end

		if thing.player and thing.player.valid
		and not thing.player.powers[pw_ignorelatch]
		and thing.tracer ~= tmthing
		and tmthing.target ~= thing then
			tmthing.boosth = thing.player.mrce.realspeed
			-- Hop on the missile for a ride!
			if not (thing.player.bot == BOT_2PAI or thing.player.bot == BOT_2PHUMAN) then
				thing.player.powers[pw_carry] = CR_GENERIC
				thing.player.pflags = $ & ~(PF_JUMPED|PF_NOJUMPDAMAGE)
				thing.tracer = tmthing
				tmthing.target = thing -- Set owner to the player
				tmthing.tracer = nil -- Disable homing-ness
				tmthing.momz = 0

				thing.angle = tmthing.angle

				-- This is required for CR_GENERIC to work
				thing.tracer = tmthing
				tmthing.boosth = max(30*FRACUNIT, thing.player.mrce.realspeed)
			end

			return false
		end
	end
end, MT_HANGGLIDER)
