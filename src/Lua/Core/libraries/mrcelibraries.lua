freeslot("SPR_7ROP", "S_DEFAULTROPE")

states[S_DEFAULTROPE] = {
	sprite = SPR_7ROP,
	frame = A|FF_PAPERSPRITE,
}

--$GZDB_SKIP
-- The sprite only can be 1 pixel width, perhaps in future..
local MRCElibs = {}

function MRCElibs.createRope(x1, y1, z1, x2, y2, z2, state)
	local rope = P_SpawnMobj(x1, y1, z1, MT_RAY)
	rope.state = state or S_DEFAULTROPE
	rope.angle = R_PointToAngle2(x1, y1, x2, y2)
	rope.flags = mobjinfo[MT_BUSH].flags

	local dx = x2-x1
	local dy = y2-y1
	local dz = z2-z1
	local dist = R_PointToDist2(x1, y1, x2, y2)
	rope.spritexscale = abs(dist) / 8


	P_SetOrigin(rope, rope.x + dx /2, rope.y + dy / 2, rope.z + dz / 2)

	rope.rollangle = InvAngle(R_PointToAngle2(rope.x, rope.z, x2, z2))
end

function MRCElibs.lerpSetObjectToPos(mobj, t, x, y, z)
	local nx = ease.linear(t, mobj.x, x)
	local ny = ease.linear(t, mobj.y, y)
	local nz = ease.linear(t, mobj.z, z)
	P_SetOrigin(mobj, nx, ny, nz)
end

function MRCElibs.lerpMoveObjectToPos(mobj, t, x, y, z)
	local nx = ease.linear(t, mobj.x, x)
	local ny = ease.linear(t, mobj.y, y)
	local nz = ease.linear(t, mobj.z, z)
	P_MoveOrigin(mobj, nx, ny, nz)
end

function MRCElibs.lerpMoveCameraToPos(t, x, y, z)
	local nx = ease.linear(t, camera.x, x)
	local ny = ease.linear(t, camera.y, y)
	local nz = ease.linear(t, camera.z, z)
	P_TeleportCameraMove(camera, nx, ny, nz)
end

function MRCElibs.backForth(t)
	return abs((t % (2 * FRACUNIT)) - FRACUNIT)
end

function MRCElibs.rad(a)
	return (AngleFixed(a)/180) % FRACUNIT
end

function MRCElibs.rawrad(a)
	return (a/180) % FRACUNIT
end

function MRCElibs.drawFakeHRotation(v, x, y, angle, scale, patch, flags, colormap)
	local fixed_angle = AngleFixed(angle)
	local pixscale = scale/patch.width
	local flip = 0

	if angle < ANGLE_90 and angle > ANGLE_270 then
		flip = V_FLIP
	end

	local rotation = max(FixedMul(scale, MRCElibs.backForth(MRCElibs.rawrad(fixed_angle) * 2) or 1), pixscale)
	v.drawStretched(x, y, rotation, scale, patch, flags|flip, colormap)
end

function MRCElibs.drawFakeHDeepRotation(v, x, y, angle, scale, patch, flags, colormap, depth)
	local fixed_angle = AngleFixed(angle)
	local flip = 0
	local half_depth = depth/2
	local pos = sin(angle)
	local pixscale = scale/patch.width

	if angle < ANGLE_90 and angle > ANGLE_270 then
		flip = V_FLIP
	end

	local rotation = max(FixedMul(scale, MRCElibs.backForth(MRCElibs.rawrad(fixed_angle) * 2) or 1), pixscale)
	v.drawStretched(x, y, rotation, scale, patch, flags|flip, colormap)
end

-- Haven't tested, nor thought through
function MRCElibs.drawFakeHCornerRotation(v, x, y, procent, scale, patch1, patch2, flags, colormap)
	local x_scale_1 = ease.linear(procent, patch1.width*scale, 0)
	local x_scale_2 = ease.linear(procent, 0, patch2.width*scale)/patch2.width

	-- Wall1
	v.drawStretched(x, y, x_scale_1/patch1.width, scale, patch, flags, colormap)

	-- Wall2
	v.drawStretched(x+x_scale_1, y, x_scale_2, scale, patch, flags, colormap)
end

-- Haven't tested, nor thought through
function MRCElibs.drawFakeHCubeRotation(v, x, y, angle, scale, wall1, wall2, flags, colormap)
	local angle_split = angle % ANGLE_180
	local side = angle_split/ANGLE_90
	local progress = MRCElibs.rad(angle)

	if side == 1 and progress > FRACUNIT then
		MRCElibs.drawFakeHCornerRotation(v, x, y, progress, scale, patch1, patch2, flags, colormap)
	else
		MRCElibs.drawFakeHCornerRotation(v, x, y, progress, scale, patch2, patch1, flags, colormap)
	end
end

function MRCElibs.drawHGradient(v, x, y, width, height, colors, flags, fixedlenght)
	local div = #colors*FRACUNIT/(fixedlenght or height)
	local last_index = -1
	local same = 0

	for i = 1, width do
		local color = colors[max(min(FixedInt(FixedRound(div*i)), #colors), 1)]
		if last_index ~= color then
			last_index = color
		elseif i < width then
			same = $+1
			continue
		end

		v.drawFill(x + i - same, y, same, height, last_index|flags)
		same = 1
	end
end

function MRCElibs.drawVGradient(v, x, y, width, height, colors, flags, fixedlenght)
	local div = #colors*FRACUNIT/(fixedlenght or width)
	local last_index = -1
	local same = 0
	for i = 1, height do
		local color = colors[max(min(FixedInt(FixedRound(div*i)), #colors), 1)]
		if last_index ~= color then
			last_index = color
		elseif i < width then
			same = $+1
			continue
		end

		v.drawFill(x, y + i - same, width, same, last_index|flags)
		same = 1
	end
end

function MRCElibs.drawSplitHGradient(v, x, y, width, height, colors, flags, fixedlenght)
	local div = #colors*FRACUNIT/(fixedlenght or height)
	local last_index = -1
	local same = 0

	for i = 1, width do
		local color = colors[max(min(FixedInt(FixedRound(div*i)), #colors), 1)]
		if last_index ~= color then
			last_index = color
		else
			same = $+1
			continue
		end

		v.drawFill(x + i - same, y, same, height, color|flags)
		same = 1
	end
end

function MRCElibs.drawHorizontalMRCELine(v, x, y, width, color)
	v.drawFill(x, y-3, width, 2, 0)
	v.drawFill(x, y-1, width, 2, color)
	v.drawFill(x, y+1, width, 2, 0)
end

function MRCElibs.drawVerticalMRCELine(v, x, y, height, color)
	v.drawFill(x-3, y, 2, height, 0)
	v.drawFill(x-1, y, 2, height, color)
	v.drawFill(x+1, y, 2, height, 0)
end

function MRCElibs.drawHorizontalClassicShadow(v, x, y, width, color)
	for i = 2,width,2 do
		v.drawFill(x + i, y, 1, i, color)
	end
end

function MRCElibs.drawVerticalClassicShadow(v, x, y, height, color)
	for i = 2,6,2 do
		v.drawFill(x + i, y, 7, height, color)
	end
end

function MRCElibs.drawMenuWindow(v, x, y, width, height, shadow_offset, color, flags)
	local corner1 = v.cachePatch('MRCEMENUC1')
	local corner2 = v.cachePatch('MRCEMENUC3')

	local shadow1 = v.cachePatch('MRCEMENUS1')
	local shadow2 = v.cachePatch('MRCEMENUS3')

	local offseted_x = x+width+corner1.width
	local offseted_y = y+height+corner1.height

	local flagcolor = color|flags

	-- SHADOW

	local sh_x = x+shadow_offset
	local sh_y = y+shadow_offset
	local sh_offset_x = offseted_x+shadow_offset
	local sh_offset_y = offseted_x+shadow_offset

	v.draw(sh_x, sh_y, shadow1, flags)

	MRCElibs.drawVerticalClassicShadow(v, sh_x, sh_y+shadow1.height+3, height, flagcolor)

	v.draw(sh_x, sh_offset_y, shadow2, flags)

	MRCElibs.drawHorizontalClassicShadow(v, sh_x+shadow1.width+2, sh_y, width, flagcolor)

	v.draw(sh_offset_x, sh_y, shadow1, flags|V_FLIP)

	MRCElibs.drawHorizontalClassicShadow(v, sh_offset_x+shadow1.width+1, sh_y, width, flagcolor)

	v.draw(sh_offset_x, sh_offset_y, shadow2, flags|V_FLIP)

	MRCElibs.drawVerticalClassicShadow(v, sh_x+shadow1.width, sh_y+shadow1.height, height, flagcolor)

	-- MAIN

	v.draw(x-1, y, corner1, flags)

	MRCElibs.drawHorizontalMRCELine(v, x+corner1.width, y+3, width, flagcolor)

	v.draw(x-1, offseted_y, corner2, flags)

	MRCElibs.drawVerticalMRCELine(v, x+3, y+corner1.height-2, height, flagcolor)

	v.draw(offseted_x, y, corner1, flags|V_FLIP)

	MRCElibs.drawHorizontalMRCELine(v, x+corner1.width, y+sh_y, width, flagcolor)

	v.draw(offseted_x, offseted_y, corner2, flags|V_FLIP)

	MRCElibs.drawVerticalMRCELine(v, x+sh_x+6, y+corner1.height, height, flagcolor)

end

function MRCElibs.vSliceDrawer(v, x, y, patch, drawf, flags, colormap, val)
	if not patch then return end
	local slice = 0

	while (slice < patch.width) do
		local slicefrac = slice << FRACBITS
		drawf(v, x, y, patch, flags, colormap, slice, slicefrac, val)

		slice = $+1
	end
end

local function drawerSliceVoxel(v, x, y, patch, flags, colormap, slice, slicefrac, val)
	local cosx = x+(slice * val)
	v.drawCropped(cosx, y, FRACUNIT, FRACUNIT, patch, flags, colormap, slicefrac, 0, FRACUNIT, patch.height*FRACUNIT)
end

local local_warning = false

function MRCElibs.voxelDrawer(v, x, y, layers, flags, colormap, angle)
	local cosine = cos(angle)
	local sine = sin(angle)

	if not layers and not local_warning then
		print('No layers, boss')
		local_warning = true
	end

	for k, layer in ipairs(layers) do
		MRCElibs.vSliceDrawer(v, x + k*sine, y, layer, drawerSliceVoxel, flags, colormap, cosine)
	end
end


function MRCElibs.drawVSlashFill(v, x, y, width, height, skips, color)
	for i = 0, width, skips do
		v.drawFill(x + i, y, 1, height, color)
	end
end

function MRCElibs.drawHSlashFill(v, x, y, width, height, skips, color)
	for i = 0, height, skips do
		v.drawFill(x, y + i, width, 1, color)
	end
end

function MRCElibs.drawHMRCESlashFill(v, x, y, width, height, skips, color1, color2)
	v.drawFill(x, y, width, height, color1)
	for i = 0, height, skips do
		v.drawFill(x - 1, y + i, width + 2, 1, color2)
	end
end

function MRCElibs.drawVMRCESlashFill(v, x, y, width, height, skips, color1, color2)
	v.drawFill(x, y, width, height, color1)
	for i = 0, width, skips do
		v.drawFill(x + i, y-1, 1, height + 2, color2)
	end
end

function MRCElibs.drawInBoundry(v, x, y, hscale, vscale, patch, xmin, xmax, ymin, ymax, flags, c)
	local crop_1 = max(ymin-y, 0)
	local crop_2 = max(ymax-y, 0)
	local crop_3 = max(xmin-x, 0)
	local crop_4 = max(xmax-x, 0)

	v.drawCropped(x, y, hscale, vscale, patch, flags, c, crop_3, crop_1, crop_4, crop_2)
end

function MRCElibs.drawProjected(v, x, y, z, offsetx, offsety, hscale, vscale, patch, flags, c)
	local projection = R_WorldToScreen2(camera, {x = x, y = y, z = z})
	local truehscale = FixedMul(projection.scale, hscale)
	local truevscale = FixedMul(projection.scale, vscale)
	local projoffsetx = FixedMul(projection.scale, offsetx)
	local projoffsety = FixedMul(projection.scale, offsety)

	v.drawStretched(projection.x + projoffsetx, projection.y + projoffsety, truehscale, truevscale, patch, flags, c)
end

function MRCElibs.drawProjectedFixed(v, x, y, z, offsetx, offsety, hscale, vscale, patch, flags, c)
	local projection = R_WorldToScreen2(camera, {x = x, y = y, z = z})
	v.drawStretched(projection.x + offsetx, projection.y + offsety, hscale, vscale, patch, flags, c)
end

-- If you want to get progress for ig various status bars scenarios
function MRCElibs.progress(x, maxx, minx)
	local offseted_x = max(min(x, maxx), minx) - minx
	local offseted_max = maxx - minx

	return offseted_x * FRACUNIT / offseted_max
end

-- Generalized spriterollangle sprite rotation -- not actually taken anything from it
-- Implementation +-borrowed from https://git.do.srb2.org/STJr/SRB2/-/merge_requests/1514#3e38d99d19d0cd959394dd380813dfdfdbbe9a30
-- Or rather it was, but I reduced it into basic trig...  It simply wasn't necessary what I was looking at.
function MRCElibs.cameraSpriteRot(mo, yaw, roll, pitch)
	local viewang = R_PointToAngle(mo.x, mo.y)
	if not R_PointToDist(mo.x, mo.y) then
		viewang = mo.angle
	end

	local ang = viewang - yaw
	mo.rollangle = FixedMul(cos(ang), roll) + FixedMul(sin(ang), pitch)
end

function MRCElibs.slopeRotBase(mo, slope)
	-- Reset
	if not slope then
		if mo.rollangle then
			mo.rollangle = ease.linear(FRACUNIT/4, mo.rollangle, FixedAngle(0))
		end
		return
	end

	MRCElibs.cameraSpriteRot(mo, slope.xydirection, 0, slope.zangle)
end

-- Use this function for automatic groundslope rotation
-- It is right now more so a macro, but it can change...
function MRCElibs.slopeRotation(mo)
	MRCElibs.slopeRotBase(mo, mo.standingslope)
end

rawset(_G, "MRCElibs", MRCElibs)
