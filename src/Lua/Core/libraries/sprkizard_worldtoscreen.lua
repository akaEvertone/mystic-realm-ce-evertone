/*
l_worldtoscreen.lua
(sprkizard)
(‎Aug 19, ‎2021, ‏‎22:51:56)
Desc: WIP

Usage: TODO
*/

-- Attempt at optimalization by Sky Dusk
--$GZDB_SKIP

local FU160 = 160 << FRACBITS
local FU100 = 100 << FRACBITS

-- vis being camera
local function R_WorldToScreen2(vis, target)
	-- Getting diffenential angle between camera and angle between camera and object
	local sx = vis.angle - R_PointToAngle2(vis.x, vis.y, target.x, target.y)
	-- Get the h distance from the target
	local hdist = R_PointToDist2(vis.x, vis.y, target.x, target.y)
	-- Visibility check
	local visible = (sx < ANGLE_90 or sx > ANGLE_270)

	-- x, y, scale, in screen POV bool
	return visible and FixedMul(FU160, tan(sx)) + FU160 or sx, -- x
	FU100 + 160*(tan(vis.aiming) - FixedDiv(target.z-vis.z, 1+FixedMul(hdist, cos(sx)))), -- y
	FixedDiv(FU160, hdist), -- scale
	visible -- in screen POV
end

rawset(_G, "R_WorldToScreen2", R_WorldToScreen2)

