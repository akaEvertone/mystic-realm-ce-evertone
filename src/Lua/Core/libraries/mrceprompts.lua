local prompts = {}

rawset(_G, "mrceprompts", {
	add = function(prompt)
		if prompt.duration and prompt.easepoint and prompt.drawer then
			-- important values
			prompt.duration = prompt.duration
			prompt.easepoint = prompt.easepoint
			prompt.drawer = prompt.drawer

			-- setup
			prompt.tic = 0
			prompt.key = #prompts+1
			prompt.offset_y = 0
			prompt.y = 0

			table.insert(prompts, prompt)
		end
	end,
})


-- Drawer
addHook("HUD", function(v)
	for key, prompt in ipairs(prompts) do
		if key > 6 then break end
		local max_ticrprompt = prompt.duration
		local easingnum = prompt.easepoint
		local awayprompt = prompt.duration - easingnum

		local current_tic = prompt.tic
		prompt.offset_y = ease.linear(min(current_tic, easingnum)*FRACUNIT/easingnum, 0, 16)
		+ease.linear(max(current_tic-awayprompt, 0)*FRACUNIT/easingnum, 16, 0)
		prompt.y = TBSlib.reachNumber(prompt.y, (key-1)*32+prompt.offset_y, 6)

		prompt.drawer(v, prompt)

		prompt.tic = $+1
		prompt.key = key
		if prompt.tic == max_ticrprompt then
			table.remove(prompts, key)
			table.sort(prompts, function(a, b)
				return a.key < b.key
			end)
		end
	end
end)