--[[

Description:
Imported Save File Manager
from Pipe Kingdom Zone's Main - g_save.lua

Contributors: Skydusk
@Team Blue Spring 2024
]]

local MRCE_Unlock = {
	game_path = "mrce/saves",
	save_system_version = "1",

	saves = {
		-- Temponary
		["TEMP_MP"] = {
			achivements = {},
			stats = {},
		}, -- Used for current multiplayer session
		["DEFAULT"] = {
			achivements = {},
			stats = {},
		}, -- Default Template

		-- Actual Saves:
		["MysticRealms"] = {
			achivements = {},
			stats = {},
			-- We need to decide what should come here.

		}, -- Save_file for Singleplayer
		-- ... (undefined, but likely more saves)
	}

}

--
--	MAIN
--

function MRCE_Unlock.loadData()
	local path = "client/"..(MRCE_Unlock.game_path).."/"..k..".dat"
	local version = true
	local check = {}

	local file = io.openlocal("client/"..(MRCE_Unlock.game_path).."/saves.dat", "r")
	if file then
		for line in file:lines() do
			table.insert(check, line)
		end
		file:close()
	end

	if check then
		for k, v in ipairs(check) do
			MRCE_Unlock.saves[v] = TBSlib.deserializeIO("client/"..(MRCE_Unlock.game_path).."/"..v..".dat")
			print('[Mystic Realms CE]'.." "..v.." Save Data Loaded!")
		end
	end

	MRCE_Unlock.saves["TEMP_MP"] = MRCE_Unlock.saves["MysticRealms"] or MRCE_Unlock.saves["DEFAULT"]
end

function MRCE_Unlock.defaultData()
	local any_default = false
	local only_default = true

	for k,v in pairs(MRCE_Unlock.saves["DEFAULT"]) do
		if not MRCE_Unlock.saves["MysticRealms"][k] then
			MRCE_Unlock.saves["MysticRealms"][k] = v
			any_default = true
		else
			only_default = false
		end
	end
	-- insert actual defaults

	if only_default then
		print('[Mystic Realms CE]'.." no standard values found, defaulting values!")
	elseif any_default then
		print('[Mystic Realms CE]'.." missing values found in save, reverting to default values!")
	end
end

function MRCE_Unlock.saveData()
	local saves = ""
	for k,v in pairs(MRCE_Unlock.saves) do
		if (k == "DEFAULT" or k == "TEMP_MP") then continue end
		TBSlib.serializeIO(v, "client/"..(MRCE_Unlock.game_path).."/"..k..".dat")
		saves = $..k.."\n"
		print('[Mystic Realms CE]'.." "..k.." Save Data Saved!")
	end

	local file = io.openlocal("client/"..(MRCE_Unlock.game_path).."/saves.dat", "w")
	if file then
		file:write(saves)
		file:close()
	end
end

function MRCE_Unlock.getSaveData()
	if multiplayer then
		return MRCE_Unlock.saves["TEMP_MP"]
	else
		return MRCE_Unlock.saves["MysticRealms"]
	end
end

function MRCE_Unlock.writeIntoSaveData(set, field, value)
	if multiplayer then
		MRCE_Unlock.saves["TEMP_MP"][set][field] = value
	else
		MRCE_Unlock.saves["MysticRealms"][set][field] = value
	end
end


MRCE_Unlock.loadData()
MRCE_Unlock.defaultData()

addHook("GameQuit", function(quit)
	if quit then
		MRCE_Unlock.saveData()
	end
end)

--
-- UTILITY
--

function MRCE_Unlock.resetProgress()
	if MRCE_Unlock.saves then
		local save_data = MRCE_Unlock.getSaveData()
		save_data = MRCE_Unlock.saves["DEFAULT"]
	end
end

--
--	MULTIPLAYER
--

-- SERVER_SAVE
addHook("GameQuit", function(quit)
	if not quit then
		if multiplayer then
			if consoleplayer == server then
				MRCE_Unlock.saves["MysticRealms"] = MRCE_Unlock.saves["TEMP_MP"]
			end
			MRCE_Unlock.saves["TEMP_MP"] = MRCE_Unlock.saves["MysticRealms"]
		end
	end
end)

-- SYNC_SERVER WITH CLIENT
addHook("NetVars", function(net)
	MRCE_Unlock.saves["TEMP_MP"] = net($)
end)

rawset(_G, "MRCE_Unlock", MRCE_Unlock)