freeslot('SPR_MECE')

--
-- GLOBAL
--

local Masochist = MRCE_Achivements.add{
	id = "Masochist",
	typein = "Task",
	name = "...",
	desc = "Are you okay?",
	tips = "Destroy 100 Eggman Monitors",
}

addHook("MobjDeath", function()
	local stats = MRCE_Unlock.getSaveData().stats

	if not stats.eggboxes then
		stats.eggboxes = 1
	else
		stats.eggboxes = $+1
	end

	if stats.eggboxes > 100 then
		MRCE_Achivements.unlock(Masochist)
	end
end, MT_EGGMAN_BOX)

--
-- DUMMIES
--

for i = 1, 50 do
	MRCE_Achivements.add{}
end -- Dummies

--
-- JCZ1
--

MRCE_Achivements.add{
	id = "JCZ1_Beach", -- unique identificator
	typein = "Medal",  -- How to get, this is not category
	set = {"JCZ", "JCZ1", "Medal"}, -- categories
	name = "First Medal!",
	desc = "Quite easy, ey?",
	tips = "In middle of starting open section...",
	inlevel = "A1", -- Merely helps with search in code and makes sure
	-- Also makes sure that unlock is only possible in said level.

	-- sprite definitions
	gamesprite = SPR_MECE,
	frame = A,
	color = SKINCOLOR_AQUAMARINE,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "JCZ1_House",
	typein = "Medal",
	set = {"JCZ", "JCZ1", "Medal"},
	name = "Beach House!",
	desc = "Definitely no creepy empty vibes.",
	tips = "Climb up first big waterfall...",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_MECE,
	frame = A,
	color = SKINCOLOR_TURQUOISE,

	hudsprite = "JCZ101",
}

MRCE_Achivements.add{
	id = "JCZ1_Ocean",
	typein = "Medal",
	set = {"JCZ", "JCZ1", "Medal"},
	name = "Monolith Over Ocean.",
	desc = "View on droplets of world",
	tips = "Out of the cave, there is worth flying somewhere...",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_MECE,
	frame = A,
	color = SKINCOLOR_SAPPHIRE,

	hudsprite = "JCZ101",
}

local JCZ1_RedCrawlas = MRCE_Achivements.add{
	id = "JCZ1_RedCrawlas",
	typein = "Task",
	set = {"JCZ", "JCZ1", "Task"},
	name = "Revenge of BLU.",
	desc = "It took 10 times of 3 and it still isn't out.",
	tips = "Destroy 30 times red crawlas in any JCZ1 playthrough...",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_MECE,
	frame = A,
	color = SKINCOLOR_PEPPER,

	hudsprite = "JCZ101",
}

addHook("MobjDeath", function(mo)
	if not (mapheaderinfo[gamemap].lvlttl == "Jade Coast" and mapheaderinfo[gamemap].actnum == 1) then return end

	local stats = MRCE_Unlock.getSaveData().stats

	if not stats.redcrawlas then
		MRCE_Unlock.writeIntoSaveData('stats', 'redcrawlas', 1)
	else
		MRCE_Unlock.writeIntoSaveData('stats', 'redcrawlas', stats.redcrawlas+1)
	end

	if stats.redcrawlas > 30 then
		MRCE_Achivements.unlock(JCZ1_RedCrawlas)
	end
end, MT_REDCRAWLA)

MRCE_Achivements.add{
	id = "JCZ1_Posters",
	typein = "Task",
	set = {"JCZ", "JCZ1", "Task"},
	name = "Someone is missing?",
	desc = "Or someone is evading paying rent.",
	tips = "Collect all posters",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_MECE,
	frame = A,
	color = SKINCOLOR_ISLAND,

	hudsprite = "JCZ101",
}

--
-- JCZ2
--

--
-- JCZ3
--

--
-- JCZ-Emerald
--

--
-- TVZ1
--

--
-- TVZ2
--

--
-- TVZ3
--

--
-- TVZ-Emerald
--

--
-- FRZ1
--

--
-- FRZ2
--

--
-- FRZ3
--

--
-- FRZ-Emerald
--

--
-- MFZ1
--

--
-- MFZ2
--

--
-- MFZ3
--

--
-- MFZ-Emerald
--

--
-- SPZ1
--

--
-- SPZ2
--

--
-- SPZ3
--

--
-- SPZ-Emerald
--

--
-- AGZ1
--

--
-- AGZ2
--

--
-- AGZ3
--

--
-- AGZ-Emerald
--

--
-- ISZ
--

--
-- PAZ1
--

--
-- PAZ2
--

--
-- PAZ3
--

--
-- MRZ
--

--
-- DWZ
--

--
-- BONUS
--

-- Leave this at very end so we could measure how many achivements we have.
print('[Mystic Realms CE] '..#MRCE_Achivements.." achivements added!")