--[[

Description:
Imported Save File Manager
from Pipe Kingdom Zone's Main - g_save.lua

Contributors: Skydusk
@Team Blue Spring 2024
]]

-- additions

local function achivementpromptdrawer(v, prompt)
	v.draw(320-128, 200-prompt.y, v.cachePatch("PROMPTGPH"), V_SNAPTORIGHT|V_SNAPTOBOTTOM)

	local icon_color = v.getColormap(TC_DEFAULT, prompt.iconcolor or SKINCOLOR_BLUE)

	if prompt.icon then
		MRCElibs.drawFakeHRotation(v, (320-121)*FRACUNIT, (223-prompt.y)*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT, v.cachePatch(prompt.icon), V_SNAPTORIGHT|V_SNAPTOBOTTOM, icon_color)
	else
		MRCElibs.drawFakeHRotation(v, (320-121)*FRACUNIT, (223-prompt.y)*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT, v.cachePatch("TABEMBICON"), V_SNAPTORIGHT|V_SNAPTOBOTTOM, icon_color)
	end

	v.drawString(320-102, 203-prompt.y, '\x8B'..'Achievement Unlocked!', V_SNAPTORIGHT|V_SNAPTOBOTTOM, "thin")
	if string.len(prompt.name) > 20 then
		local string_1 = string.sub(prompt.name, 1, 20)
		local string_2 = string.sub(prompt.name, 21, string.len(prompt.name))
		if string.len(string_2) > 20 then
			string_2 = string.sub(string_2, 1, 17).."..."
		end

		v.drawString(320-102, 211-prompt.y, string_1, V_SNAPTORIGHT|V_SNAPTOBOTTOM, "thin")
		v.drawString(320-102, 219-prompt.y, string_2, V_SNAPTORIGHT|V_SNAPTOBOTTOM, "thin")

	else
		v.drawString(320-102, 211-prompt.y, prompt.name, V_SNAPTORIGHT|V_SNAPTOBOTTOM, "thin")
	end
end

-- main

local achive_info = {unlocked = 0,}
local identificators = {}
local id_leveldatabase = {}
local id_categorydb = {global = {}}

local non_extra_total = 0

function achive_info.add(info)
	if type(info) ~= "table" then
		error('[Add Achivement] Not a table')
		return
	end

	-- setup temp
	local index = {}

	-- Metadata

	if info.name then
		index.name = tostring(info.name)
	else
		index.name = "NULL"
	end

	if info.desc then
		index.desc = tostring(info.desc)
		if info.minidesc then
			index.minidesc = tostring(info.desc)
		end
	elseif info.minidesc then
		index.desc = tostring(info.minidesc)
		index.minidesc = tostring(info.desc)
	else
		index.desc = "NULL"
	end

	if info.tips then
		index.tips = tostring(info.tips)
	end

	-- active upon unlocking

	if info.func then
		index.func = info.func
	end

	-- spriteinfo

	if info.gamesprite and type(info.gamesprite) == 'number' then
		index.gamesprite = info.gamesprite
	end

	if info.hudsprite and type(info.hudsprite) == 'string' then
		index.hudsprite = info.hudsprite
	end

	if info.frame then
		index.frame = info.frame
	else
		index.frame = A
	end

	if info.color then
		index.color = info.color
	else
		index.color = SKINCOLOR_BLUE
	end

	-- Properties

	index.flags = 0	 -- compression of flags if necessary
	if info.hidden then
		index.flags = $|1
	end

	if info.typein and type(info.typein) == "string" then
		index.typein = info.typein
	else
		index.typein = "Generic"
	end

	if info.requirements then
		if type(info.requirements) == "table" then
			index.requirements = info.requirements
		else
			error('[Add Achivement] Requirements - Not a table')
		end
	end

	if info.inlevel then
		local num = info.inlevel

		if type(info.inlevel) == "string" then
			num = G_FindMapByNameOrCode(info.inlevel)
		end

		if type(num) == "number" then
			if not id_leveldatabase[num] then
				id_leveldatabase[num] = {}
			end
			table.insert(id_leveldatabase[num], #achive_info+1)

			index.inlevel = num
		end
	end

	if info.id then
		index.id = info.id
		identificators[info.id] = #achive_info + 1
	else
		index.id = "NULL"
	end

	-- This is in order to be done first
	-- extra is shadow achivements that do not count to %
	if info.extras then
		index.extras = true
	else
		non_extra_total = $+1
		index.extra = false
	end

	-- Categorization
	if info.set then
		if type(info.set) == "string" then
			index.set = info.set
			if not id_categorydb[info.set] then
				id_categorydb[info.set] = {}
			end

			table.insert(id_categorydb[info.set], #achive_info + 1)
		elseif type(info.set) == "table" then
			local sets = info.set
			for i = 1, #sets do
				local set = sets[i]
				if type(set) == "string" then
					index.set = info.set
					if not id_categorydb[set] then
						id_categorydb[set] = {}
					end

					table.insert(id_categorydb[set], #achive_info + 1)
				end
			end
		else
			index.set = "Global"
			table.insert(id_categorydb.global, #achive_info + 1)
		end
	else
		index.set = "Global"
		table.insert(id_categorydb.global, #achive_info + 1)
	end


	table.insert(achive_info, index)
	return #achive_info
end

function achive_info.searchByName(name)
	for id = 1, #achive_info do
		local entry = achive_info[id]
		if name == entry.name then
			return entry, id
		end
	end
end

function achive_info.searchByID(id)
	if identificators[id] and achive_info[identificators[id]] then
		return achive_info[identificators[id]], identificators[id]
	end
end

function achive_info.searchByCategory(category)
	if id_categorydb[category] then
		local array = id_categorydb[category]
		local result = {}

		for i = 1, #array do
			table.insert(result, array[i])
		end

		return result
	end
end

function achive_info.recount()
	local save = MRCE_Unlock.getSaveData().achivements
	local result = 0

	for _,v in pairs(save) do
		if v then
			result = $+1
		end
	end

	achive_info.unlocked = result
	return result
end

function achive_info.isUnlocked(id)
	local save = MRCE_Unlock.getSaveData().achivements
	if save[id] then
		return true
	else
		return false
	end
end

function achive_info.progress()
	return achive_info.unlocked*FRACUNIT/non_extra_total
end

function achive_info.unlock(query_id)
	local info_id = query_id
	if type(info_id) == "string" then
		local entry
		entry, info_id = achive_info.searchByID(info_id)
	end

	if type(info_id) == "number" and achive_info[info_id] then
		local achivement = achive_info[info_id]
		local save = MRCE_Unlock.getSaveData().achivements

		if achivement.requirements then
			for _,v in ipairs(achivement.requirements) do
				if not save[v] then
					return
				end
			end
		end

		if achivement.inlevel and gamemap ~= achivement.inlevel then
			return
		end

		if not save[info_id] then
			save[info_id] = true -- save existence
			achive_info.recount()
			--print('Achivement Unlocked! | '..(achivement.name)..' | '..(achivement.desc))

			mrceprompts.add{
				duration = 8*TICRATE,
				easepoint = TICRATE/2,
				drawer = achivementpromptdrawer,
				name = achivement.name,
				icon = achivement.hudsprite,
				iconcolor = achivement.color
			}

			if achivement.func then
				achivement.func()
			end
		end
	end
end

-- Debug command to list all achivements.
COM_AddCommand("debug_mrce_achivementlist", function()
	local save = MRCE_Unlock.getSaveData().achivements
	for k, v in ipairs(achive_info) do
		if v.name == "NULL" then continue end
		print(k..' '..(v.id)..'. Achivement | Name: '..(v.name)..' | Unlocked?: '..tostring(save[k])..' | Tip: '..tostring(v.tips))
	end
	print('In total: '..#MRCE_Achivements.." achivements.")
end, COM_LOCAL)

-- Debug command to list all achivements.
COM_AddCommand("debug_mrce_insertfakeachiprompt", function(p, var1, var2)
	mrceprompts.add{
		duration = 8*TICRATE,
		easepoint = TICRATE/2,
		drawer = achivementpromptdrawer,
		name = tostring(var1)
	}
end, COM_LOCAL)

-- Initial Achivement Test!
local inital_a = achive_info.add{
	name = "Game Loaded!",
	desc = "Load game, dummy!",
}

achive_info.unlock(inital_a)
achive_info.recount()

-- Achivement Granting General Object
local mobjinfo_id = freeslot("MT_MRCEACHIVEMENT")

mobjinfo[mobjinfo_id] = {
--$Category MRCE Generic
--$Name Achivement Medal
--$Sprite BOM1A0
--$Arg0 Order
--$Arg1 Frame
	doomednum = 831,
	spawnstate = S_EMBLEM1,
	spawnhealth = 1000,
	reactiontime = 8,
	deathstate = S_SPRK1,
	deathsound = sfx_ncitem,
	speed = 1,
	radius = 16*FRACUNIT,
	height = 30*FRACUNIT,
	mass = 4,
	flags = MF_SPECIAL|MF_NOGRAVITY|MF_NOCLIPHEIGHT
}

addHook("MapThingSpawn", function(mo, mt)
	if id_leveldatabase[gamemap] and id_leveldatabase[gamemap][mt.args[0]] then
		mo.extravalue1 = id_leveldatabase[gamemap][mt.args[0]]
		local save = MRCE_Unlock.getSaveData().achivements
		if save[mo.extravalue1] then
			mo.extravalue2 = 1
		end
	end
	if mt.stringargs[1] then
		local entry
		entry, mo.extravalue1 = achive_info.searchByID(mt.stringargs[1])
		local save = MRCE_Unlock.getSaveData().achivements
		if save[mo.extravalue1] then
			mo.extravalue2 = 1
		end

		if entry.gamesprite then
			mo.sprite = entry.gamesprite
		end

		if entry.frame then
			mo.frame = entry.frame
		end

		if entry.color then
			mo.color = entry.color
		end
	end

	if mt.stringargs[0] then
		if _G[mt.stringargs[0]] and skincolors[_G[mt.stringargs[0]]] then
			mo.color = skincolors[_G[mt.stringargs[0]]]
		end
	end

	if mo.extravalue2 then
		mo.frame = $|FF_TRANS50
	end
end, mobjinfo_id)

addHook("MobjSpawn", function(emblem)
	emblem.origz = emblem.z
end, mobjinfo_id)

addHook("MobjThinker", function(emblem)
	if not (emblem and emblem.valid) then return end
	if not (emblem.frame & FF_PAPERSPRITE) then
		emblem.frame = $|FF_PAPERSPRITE
	end
	emblem.angle = $ + FixedAngle(FRACUNIT)
	emblem.z = emblem.origz + 8 * abs(sin(FixedAngle(leveltime*4*FRACUNIT)))
	local emblemparticle = P_SpawnMobjFromMobj(emblem, P_RandomRange(-10, 10) * FRACUNIT, P_RandomRange(-10, 10) * FRACUNIT, 20*FRACUNIT, MT_SUPERSPARK)
	emblemparticle.momx = P_RandomRange(-2, 2) * FRACUNIT
	emblemparticle.momy = P_RandomRange(-2, 2) * FRACUNIT
	emblemparticle.momz =  P_RandomRange(-2, 2) * FRACUNIT
	emblemparticle.colorized = true
	emblemparticle.color = emblem.color
	emblemparticle.fuse = 10
	emblemparticle.scale = emblem.scale *1/6
	emblemparticle.source = emblem
	if (emblem.frame & FF_TRANSMASK) then
		emblemparticle.flags2 = MF2_DONTDRAW
	end
end, mobjinfo_id)

addHook("TouchSpecial", function(mo)
	if mo.extravalue2 then
		return true
	end

	if mo.extravalue1 and not mo.extravalue2 then
		achive_info.unlock(mo.extravalue1)
		mo.extravalue2 = 1
	end
end, mobjinfo_id)

addHook("LinedefExecute", function(line, mo)
	if line.args[0] then
		achive_info.unlock(line.args[0])
	end

	if line.stringargs[1] then
		achive_info.unlock(line.stringargs[1])
	end
end, "MRCEACHI")

rawset(_G, "MRCE_Achivements", achive_info)