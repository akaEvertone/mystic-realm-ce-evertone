--[[Lite version of the new momentum script by rumia1. (full version name pending)
This script may freely be reused, reproduced, or edited by
the Mystic Realm CE team for the Mystic Realm CE project.

If you wish to use this script for your own projects or level packs,
please wait for the full release which will be marked as reusable]]
for i = 1, 7, 1 do
	freeslot("sfx_rev" .. i)
end

local charactersupport = {
	["sonic"] = {true, true, 0, true},
	["tails"] = {true, true, 0, false},
	["knuckles"] = {true, true, 0, false},
	["sms"] = {false, false, 1, false},
	["adventuresonic"] = {false, false, 1, false},
	["blaze"] = {true, true, 1, false},
	["juniosonic"] = {false, false, 1, false},
	["iclyn"] = {false, false, 1, false},
	["ass"] = {false, true, 1, false},
	["aether"] = {false, true, 1, false},
	["inazuma"] = {false, true, 1, false},
	["amy"] = {true, false, 1, false},
	["mario"] = {true, false, 1, false},
	["luigi"] = {true, false, 1, false},
	["kiryu"] = {false, false, 1, false},
	["metaknight"] = {true, false, 1, false},
	["bandages"] = {false, false, 1, false},
	["flarethetrex"] = {false, false, 1, false},
	["bowser"] = {true, false, 1, false},
	["pointy"] = {true, false, 1, false},
	["fluffy"] = {true, false, 1, false},
	["samus"] = {false, false, 1, false},
	["basesamus"] = {false, false, 1, false},
	["rosegoldamy"] = {true, false, 1, false}
}

rawset(_G, "mrceCharacterPhysics", function(skin, momentum, stats, spindash, run2)
	local s = string.lower(skin)
	if charactersupport[s] ~= nil then return end
	local r2 = run2 or false --peel anim default to false
	charactersupport[s] = {momentum, stats, spindash, run2} --table insert doesn't like using a string as the position. makes sense ig but I don't care about position or order I just need to add entries
	print("Added CharacterPhysics support for skinname \130" .. s)
	local momcolor, statscolor, spincolor, peelcolor = momentum and "\131" or "\133", stats and "\131" or "\133", spindash and "\133" or "\131", r2 and "\131" or "\133"
	print("Data set \138Momentum: " .. momcolor .. tostring(momentum) .. " \138Stats: " .. statscolor .. tostring(stats) .. " \138Spindash: " .. spincolor .. tostring(spindash) .. " \138Peelout Animation: " .. peelcolor .. tostring(r2) "\n")
end)

local function P_SpawnCoolSkidDust(player, radius, sound, state)
    local particle = P_SpawnMobjFromMobj(player.mo, 0, 0, 0, MT_SPINDUST)
    local xn = P_RandomChance(FRACUNIT/2)
    local yn = P_RandomChance(FRACUNIT/2)
    if xn then xn = -1 else xn = 1 end
    if yn then yn = -1 else yn = 1 end

    local x = particle.x + (xn * (FixedMul(radius,P_RandomFixed()) << FRACBITS))
    local y = particle.y + (yn * (FixedMul(radius,P_RandomFixed()) << FRACBITS))
    local z = particle.z --+ (P_RandomRange(0, FRACUNIT-1))
    P_MoveOrigin(particle, x, y, z)
    particle.tics = 10
    particle.scale = (2*player.mo.scale)/3
    particle.momx = player.mo.momx/4
    particle.momy = player.mo.momy/4
    P_SetScale(particle, particle.destscale)
    P_SetObjectMomZ(particle, FRACUNIT, false)
    if player.powers[pw_super] then
        P_SetObjectMomZ(particle, FRACUNIT*3, false)
    end

    if sound then
        S_StartSound(player.mo, sfx_s3k7e)
    end
    return particle -- the one thing the original version of this didn't do, smh smh
end

addHook("ThinkFrame", function()
	for player in players.iterate do
		if player.mo and player.mo.valid then --standard opener
			--------------------------------------------------
			--              CUSTOM VARIABLES                --
			--------------------------------------------------
			player.rspeed = FixedHypot(player.rmomx, player.rmomy) --gives the player's accurate speed in FU/T, kinda necessary for the entire project to function

			if player.mo.prevz == nil then --the previous frame's z coordinate, used for calculating slope acceleration
				player.mo.prevz = 0
			end

			player.mo.relz = player.mo.z*P_MobjFlip(player.mo) --relative z, used in gravity-sensitive situations

			player.mo.rmomz = player.mo.momz*P_MobjFlip(player.mo) --relative momentum z, used in gravity-sensitive situations

			player.mo.rprevz = player.mo.prevz*P_MobjFlip(player.mo) --same as relative z but in the past



			if player.prevnormalspd == nil then --the previous frame's normalspeed value, used for setting the current speed of the player
				player.prevnormalspd = 0
			end

			if player.mo.prevjumpfactor == nil then --the player's last known jumpfactor, used for the spindash to reset the player's jumpfactor
				player.mo.prevjumpfactor = 0
			end

			--if player.runspeed2 == nil then --sets the player's run2 speed, intended to be used like player.runspeed for a second, faster run animation.
				--[[player.runspeed2 = 60*FU the default run2 speed
				intended to be a fallback for character creators that have not input their own value.
				no longer used as peel will be used as a visual indicator for water running speed, which is always 60*FU/T

				Defaults
					Sonic - 60 FRACUNITs
					Tails - 42 FRACUNITs]]
			--end


			if player.mo.gpe_hasrun2 == nil then --skin toggle for run2 animation
			--used to toggle if the skin's second run animation is enabled (think the peelout or tails' faster tail animation in sonic 2)
			--(currently unused in the main script at the time of writing)
				player.mo.gpe_hasrun2 = false
			end

			if player.mo.gpe_hasmomentum == nil then --skin toggle for momentum and slope acceleration
				player.mo.gpe_hasmomentum = true
			end

			if player.mo.gpe_statchanges == nil then --skin toggle for stat overrides
			--(note that at the moment thrustfactor is hardcoded due to being required for proper momentum calculation)
				player.mo.gpe_statchanges = true --if the skin creator sets this to false, the script will fallback on skin defined stats
			end


			if player.mo.gpe_sdstyle == nil then --skin toggle for the player's spindash style
			--0 is the new mash-style spindash, 1 is the unmodified vanilla behavior.
				player.mo.gpe_sdstyle = 0
			end

			if player.mo.spincharge == nil then --the number of charges the spindash has, will be used for calculating the sound to use for each level of spindash
				player.mo.spincharge = 0
			end

			if player.mo.jumpdown == nil then --thinkframe compatible version of what PF_JUMPDOWN is supposed to do
				player.mo.jumpdown = false
			end
			if player.mo.usedown == nil then --thinkframe compatible version of what PF_USEDOWN is supposed to do
				player.mo.usedown = false
			end

			player.spindiv = (player.maxdash - player.mindash) / 8 --makes working with the mindash and maxdash values easier

		--------------------------------------------------
		--              STAT OVERRIDES                  --
		--------------------------------------------------
			if (player.charflags & SF_DASHMODE) and (player.dashmode > 3*FU) then
				player.defaultspeed = ((player.mo.gpe_statchanges and 36*FU) or skins[player.mo.skin].normalspeed) + ((player.dashmode-3*FU/3)*FU)
			else
				player.defaultspeed = ((player.mo.gpe_statchanges and 36*FU) or skins[player.mo.skin].normalspeed)
			end
			--our stand in for the skin's normalspeed value, since normalspeed is being used to modify how fast we can go in this script
			--36 FRACUNITs is the default based on the new skin standards used in vanilla, but this value can freely be modified.
			--if the skin maker chooses to disable gpe_statchanges, this value will use their skin's defined normalspeed as a fallback
			--a small amount of the player's dashmode value is added to this as a speed if they're in dashmode

			player.topspeed = 178*FU --speed cap
			--i'm not actually sure if the classics have a ground speed cap
			--this is the hard limit for how fast you can go either by rolling
			--or by running down/jumping off of slopes
			if player.mo.gpe_hasmomentum == true then
				player.thrustfactor = ((player.powers[pw_sneakers] or player.powers[pw_super]) and 6 or 5)
			end
			--the player's thrustfactor is modified based on their super or sneaker status to change the rate of acceleration
			--THIS VALUE IS HARDCODED AS CHANGES TO THIS VALUE WILL BREAK MOMENTUM CALCULATION
			--UNLESS THE MOMENTUM SCRIPT IS UPDATED

			if player.mo.gpe_statchanges == true then
			--if the skin creator has enabled this script to change stats to match the rest of the cast,
			--these values will be used instead of their skin values
				player.accelstart = 96
				player.acceleration = 46
				player.mindash = 30*FU
				player.maxdash = 70*FU
			end

		--------------------------------------------------
		--  			MOMENTUM PHYSICS                --
		--------------------------------------------------


			--momentum retention
			if player.mo.gpe_hasmomentum == true and player.mrce.physics then
				--speed adding/retention
				if P_IsObjectOnGround(player.mo)
				and not (player.powers[pw_super]) then --thank goodness i don't think i actually have to do anything here in order for super to work...
					if player.rspeed > ( --if the player is going faster than...
					(player.powers[pw_sneakers] and not (player.mo.flags2 & MF2_TWOD) and player.normalspeed*2) -- two times speed for speedshoes,
					or (((player.powers[pw_sneakers] and player.mo.flags2 & MF2_TWOD) and (9*player.normalspeed/6)*2) or player.mo.flags2 & MF2_TWOD and 9*player.normalspeed/6) --1.5 times speed for 2d flag, and twice that for speedshoes...
					or player.normalspeed) then --... or the player is just going faster than their normalspeed
						player.normalspeed = player.rspeed + (player.normalspeed/9) --set their normalspeed higher, note that the extra boost from normalspeed/9 is to account for the differential from friction and scale
					end
				end


				--i really don't like TOL_2D


				--speed loss
				if player.normalspeed > player.defaultspeed --if the player is going faster than they can normally
				and player.rspeed < ((player.powers[pw_sneakers] and 6*player.normalspeed/4) or player.normalspeed) - (player.normalspeed/9) then --and they're slowing down
					player.normalspeed = $ - (FU*6) --reduce their max speed
				end
				--speed reset
				if player.normalspeed <= player.defaultspeed --if the player is going less than their top speed
				or player.mo.state == S_PLAY_SPRING --reset the player's speed on springs
				or P_PlayerInPain(player) --reset the player's speed if they're hurt
				or player.playerstate == PST_DEAD then --reset the player's speed if they're dead, just in case
					player.normalspeed = player.defaultspeed -- reset the player's normalspeed
				end


				--slope acceleration
				if player.mo.rprevz > player.mo.relz
				and P_IsObjectOnGround(player.mo) --And the player is on the ground (kinda important)
				and player.rspeed < player.topspeed --Don't accelerate if we're over max speed, this keeps control managable
				and player.mo.standingslope ~= nil then
					if not (player.mo.flags2 & MF2_TWOD) then --if we aren't currently in a 2d section
						player.normalspeed = player.prevnormalspd + (player.acceleration * player.thrustfactor/46)*(player.mo.rprevz - player.mo.relz)/100 --add the slope's drop plus the player's acceleration accounting for thrustfactor
					else
						player.normalspeed = player.prevnormalspd + 2*((player.acceleration * player.thrustfactor/46)*(player.mo.rprevz - player.mo.relz)/100)/3 --if we are in 2d, use 2/3 that speed
					end
				end


				--rolling physics
				if player.pflags & PF_SPINNING
				and leveltime > 0
				and not (player.pflags & PF_STARTDASH)
				and P_IsObjectOnGround(player.mo) then
					if player.mo.rprevz ~= player.mo.relz --If we're on a slope...
					and player.rspeed < player.topspeed
					and player.mo.standingslope ~= nil then
						P_Thrust(player.mo, R_PointToAngle2(0, 0, player.rmomx, player.rmomy) + (player.rspeed*player.cmd.sidemove*ANGLE_180), (player.mo.rprevz - player.mo.relz) / 15) --modify the amount this is divided by to change how much rolling is affected
					else --If we're on flat ground...
						P_Thrust(player.mo, R_PointToAngle2(0, 0, player.rmomx, player.rmomy) + (player.rspeed*player.cmd.sidemove*ANGLE_180), (player.rspeed/200) - (player.mo.friction/3)) --i did my best to match vanilla's rolling here
					end
				end


			end


		--------------------------------------------------
		--  			  GAMEPLAY CHANGES              --
		--------------------------------------------------

			--Making it so you can hold spin to roll out of a thok action
			if player.charability2 == CA2_SPINDASH
			and not (player.charability == CA_GLIDEANDCLIMB)
			and (player.cmd.buttons & BT_USE)
			and (player.mo.eflags & MFE_JUSTHITFLOOR)
			and not (player.pflags & PF_SHIELDABILITY and player.pflags & PF_THOKKED and player.powers[pw_shield] & SH_BUBBLEWRAP)
			and player.mo.state ~= S_PLAY_DEAD
			and not P_CheckDeathPitCollide(player.mo)
			and not player.mo.sprung
			and not player.mrce.elemdive then
				player.pflags = $1 | PF_SPINNING
				player.mo.state = S_PLAY_ROLL
				if (player.rspeed > (6*player.mo.scale)) then
					S_StartSound(player.mo, sfx_spin)
				end
			end

			--Removes the roll lock you get from jumping out of a spin
			--KNOWN ISSUE: due to the thokked flag being linked to the shield ability flag,
			--this makes it so you only have one frame to do a whirlwind shield jump if you roll off a slope

			if not P_IsObjectOnGround(player.mo)
			and player.mo.gpe_sdstyle == 0
			and player.pflags & PF_SPINNING
			and player.mrce.physics then
				player.pflags = $ & ~PF_SPINNING
				player.pflags = $ | PF_JUMPED
				if not (player.pflags & PF_STARTJUMP) then
					player.pflags = $ | PF_THOKKED
				end
			end

			--mash-style spindash behavior

			if player.charability2 == CA2_SPINDASH --if the player can spindash
			and player.mrce.physics
			and player.mo.gpe_sdstyle == 0 then --and the player's skin is using this style

				if player.cmd.buttons & BT_SPIN --if the player is pressing spin
				and P_IsObjectOnGround(player.mo)--and the player is on the ground
				and (player.speed < FixedMul(5<<FRACBITS, player.mo.scale) or player.mo.state ~= S_PLAY_GLIDE_LANDING)-- and the player satisfies the conditions to need to spindash
				and (player.mo.state == S_PLAY_SPINDASH) then --eh to hell with it let's make sure the player is in their spindash animation too

					if player.mo.prevjumpfactor == 0 then --if the player's previous jumpfactor is at 0
						player.mo.prevjumpfactor = player.jumpfactor --store the player's previous jumpfactor for later
					end
					if (player.pflags & PF_STARTDASH) then
						player.jumpfactor = 0 --set the actual jumpfactor value to 0 so we can't jump out of the spindash
						if player.mrce.dashcancel >= 12 then
							player.jumpfactor = player.mo.prevjumpfactor
							player.mo.prevjumpfactor = 0
							P_DoJump(player, true)
						end
					end


					player.dashspeed = (player.mo.gpe_statchanges and player.mindash or skins[player.mo.skin].mindash) + (player.spindiv*player.mo.spincharge) --increase the player's dash speed by 1/8th of the difference between their mindash and maxdash
					if player.cmd.buttons & BT_JUMP --if the player is is pressing the jump button
					and not player.mo.jumpdown then --and jump was not held on the previous frame
						if player.mo.spincharge < 8 then --if we haven't hit our charge limit
							player.mo.spincharge = $ + 1 -- increase the player's spincharges by 1
						end
						if player.mo.spincharge > 1 then
							S_StopSound(player.mo)--stop the player's current sound
							S_StartSound(player.mo, sfx_rev1 + min(player.mo.spincharge - 1, 6))--play the spindash sound
						else
							S_StopSoundByID(player.mo,sfx_spndsh)--stop the player's current sound
							S_StartSound(player.mo,sfx_spndsh)--play the spindash sound
						end

					end
					if player.mo.spincharge < 4 and not (player.mrce.spin % 20) then
						player.mo.spincharge = $ + 1 -- increase the player's spincharges by 1
						S_StopSoundByID(player.mo,sfx_spndsh)--stop the player's current sound
						S_StartSound(player.mo,sfx_spndsh)--play the spindash sound
					end
				else
					if player.jumpfactor < player.mo.prevjumpfactor --if the player's jumpfactor is less than the one prior to starting a spindash
					and not (player.pflags & PF_STARTDASH) then --and we're no longer spindashing
						player.jumpfactor = player.mo.prevjumpfactor --reset the value to before we spindashed
						player.mo.prevjumpfactor = 0 --set the storage value back to 0
					end
					player.mo.spincharge = 0 --reset the number of spin charges
				end

			end


		--------------------------------------------------
		--  				ANIMATION	 	       		--
		--------------------------------------------------


			--run2 animations
			if player.mo.state == S_PLAY_DASH
			or (player.mo.state == S_PLAY_RUN and player.rspeed >= 60*FRACUNIT) then
				if player.mo.tics > 1 then
					player.mo.tics = 1
				end
			end

			if player.mo.gpe_hasrun2 then --if the player has a fast run animation set up
				if P_IsObjectOnGround(player.mo) then --if the player is on the ground
					if ((player.powers[pw_super] and player.mrce.realspeed >= 80*FRACUNIT) or (player.mrce.realspeed >= 60*FRACUNIT and not player.powers[pw_super])) --and their speed exceeds waterrun threshold
					and player.panim == PA_RUN then --and they're in the running animation
						player.mo.state = S_PLAY_DASH --set their state to the dash state
					end

					if ((player.powers[pw_super] and player.mrce.realspeed <= 80*FRACUNIT) or (player.mrce.realspeed <= 60*FRACUNIT and not player.powers[pw_super])) --if the player's speed no longer exceeds waterrun threshold
					and not player.skidtime then --and they're not skidding
						if player.panim == PA_DASH then --if we're still using the dash animation
							player.mo.state = S_PLAY_RUN --use the running state instead
						end
					end
				end
			end

		--------------------------------------------------
		--  				  MISC	 		            --
		--------------------------------------------------
			player.mo.prevz = player.mo.z
			player.prevnormalspd = player.normalspeed
			player.mo.jumpdown = (player.cmd.buttons & BT_JUMP)
			player.mo.usedown = (player.cmd.buttons & BT_SPIN)
		end
	end
end)

addHook("PlayerThink", function(p)
	local x = p.mrce
	if p.spectator then return end
--detect if physics are enabled
	if IsCustomSkin(p) then
		x.physics = false
	else
		x.physics = true
	end
--disable custom physics for a few known characters
	--if p.mo and ((p.mo.skin == "sms") or (p.mo.skin == "ass") or (p.mo.skin == "juniosonic") or (p.mo.skin == "iclyn") or (p.mo.skin =="kiryu") or (p.mo.skin == "adventuresonic")) then
	--	x.customskin = 2
	--	x.physics = false
	--elseif x.dontwantphysics == false then
	--	x.physics = true
	--end
	if p.cmd.forwardmove <= -38 and x.jump and (p.pflags & PF_STARTDASH) then
		x.dashcancel = min($ + 1, 12)
	else
		x.dashcancel = 0
	end

	--move this down here since playerthink runs before thinkframe, might help fix momentum breaking in netgames when switching characters a lot
	for k, v in pairs(charactersupport) do
		if p.mo.skin ~= k then continue end
		if v[1] == false then p.mo.gpe_hasmomentum = false else p.mo.gpe_hasmomentum = nil end
		if v[2] == false then p.mo.gpe_statchanges = false else p.mo.gpe_statchanges = nil end
		if v[3] ~= 0 then p.mo.gpe_sdstyle = v[3] else p.mo.gpe_sdstyle = nil end
		if v[4] ~= nil and v[4] == true then p.mo.gpe_hasrun2 = true else p.mo.gpe_hasrun2 = nil end
	end
	--print(x.dashcancel)
	--if p.speed == go fast, allow the player to run on water. aka, momentum based water running
	if (x.realspeed >= 60*FRACUNIT) and not (skins[p.mo.skin].flags & SF_RUNONWATER) and not IsCustomSkin(p)
	and not ((p.mo.skin == "mario" or p.mo.skin == "luigi") and p.powers[pw_shield] == SH_MINI) --mini mario gets to always run on water
	and not (p.mo.skin == "inazuma" or p.mo.skin == "aether") then
		p.charflags = $1|SF_RUNONWATER
		if p.mo.eflags & (MFE_TOUCHWATER|MFE_UNDERWATER) and P_IsObjectOnGround(p.mo) then
			local sploop = P_SpawnCoolSkidDust(p, 16, false, nil)
			sploop.destscale = p.mo.scale/6+p.mo.scale/5
			sploop.scalespeed = FRACUNIT/19
			sploop.state = S_SPLISH1
		end
	elseif not IsCustomSkin(p) then
		if not (skins[p.mo.skin].flags & SF_RUNONWATER)
		and not ((p.mo.skin == "mario" or p.mo.skin == "luigi") and p.powers[pw_shield] == SH_MINI)
		and not (p.mo.skin == "inazuma" or p.mo.skin == "aether")
		and not (p.solchar and p.solchar.istransformed)
		and x.realspeed <= 60*FRACUNIT then
			p.charflags = $1 & ~(SF_RUNONWATER)
		end
	end
end)
addHook("NetVars", function(net)
	charactersupport = net($)  --net sync that shit
end)