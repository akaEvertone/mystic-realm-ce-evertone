addHook("MobjThinker",function(mo)
	if not mo.target then
		P_RemoveMobj(mo)
		return
	end

	if P_IsObjectOnGround(mo.target)
	or mo.target.player.powers[pw_shield] != SH_CLOCK then
		P_RemoveMobj(mo)
		return
	end
end, MT_CLOCK_REWIND)

addHook("MobjRemoved",function(mo)
-- bottomless pits suck
	if mo.tracer then
		mo.tracer.clockrw = nil
	end
	if mo.target then
		mo.target.rewindpoint = nil
		mo.target = nil
	end
end, MT_CLOCK_REWIND)