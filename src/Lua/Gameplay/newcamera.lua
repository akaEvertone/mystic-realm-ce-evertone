-- https:--git.do.srb2.org/STJr/SRB2/-/blob/next/src/p_mobj.c#L3515
-- merely reference, to see how camera works.

-- As of now, camera gets stuck quite often and some precautions would be perhaps necessary (no solution found yet)
-- Reason why this script even exists, to smooth out camera to be a little bit more functional. (no adjustments to real camera yet)
-- Similarly, to allow for 'special interests' (are a thing)

-- To allow for additions
local camextras_class = {
	interest = nil,
	int_stay = false, -- in distance of interest, camera will stop moving on it's own.
	int_move = false, -- adjustment of camera based on movement.
	int_invm = false, -- inverts order in the int_move.
	int_dist = 0, -- not datatype int (fixed_t), this is merely required distance of interest

	path_act = false,
	temppaths = {}, --@class {x, y, z, angle, aiming, speed, [action]}, -- paths that are strict in use (useful for cutscenes)
}

local playerpaths = {} --@class {x, y, z, direction, radius, next}, -- paths that still make player a focus (auto camera sections,
-- technically they aren't even paths but it will search for 2 nearest points and then interpolated closest point to player between them)


local interests = {
	--@class {interests, [int_stay], [int_move], [int_invm], int_dist}
}

local cam_variables = {
	[camera] = camextras_class,
	--[camera2] = camextras_class,
}

local function P_CameraAdditions(cam, player)
	local vars = cam_variables[cam]
	local mo = player.realmo

	if not mo then return end
	local mo_dist = R_PointToDist2(cam.x, cam.y, mo.x, mo.y)
	local dist = INT32_MAX

	-- automatic interests search
	if interests then
		for i = 1, #interests do
			local interest_obj = interests[i]
			local compdist = R_PointToDist2(cam.x, cam.y, interest_obj[1].x, interest_obj[1].y)
			if compdist < dist then
				vars.interest = interest_obj[1]
				vars.int_stay = interest_obj[2]
				vars.int_move = interest_obj[3]
				vars.int_invm = interest_obj[4]
				vars.int_dist = interest_obj[5]
			end
		end
	end

	-- interest automatic lock-on
	-- please never use it on homing lock on or anything stupid like that
	-- it would be horrible if camera autoadjusted for every target
	-- this is meant for important events.
	if vars.interest then
		if vars.interest.valid then
			local interest = vars.interest

			if dist < vars.int_dist then
				local aim_pitch = R_PointToAngle2(0, 0, dist, interest.z - cam.z)
				local aim_angle = R_PointToAngle2(cam.x, cam.y, interest.x, interest.y)

				local mo_pitch = R_PointToAngle2(0, 0, mo_dist, mo.z - cam.z)
				local mo_angle = R_PointToAngle2(cam.x, cam.y, mo.x, mo.y)

				local ang_dif = aim_angle - mo_angle
				local aim_dif = aim_pitch - mo_pitch

				if abs(ang_dif) < abs(ANGLE_90) and abs(aim_dif) < abs(ANGLE_90) then
					cam.aiming = ease.linear(FRACUNIT/20, cam.aiming, mo_pitch+aim_dif)
					cam.angle = ease.linear(FRACUNIT/20, cam.angle, mo_angle+ang_dif)

					vars.int_stay = vars.int_move
					if vars.int_stay == true then
						cam.momx = 0
						cam.momy = 0
					end

					if vars.int_move == true then
						local distinbtw = R_PointToDist2(mo.x, mo.y, interest.x, interest.y)/2
						local angleinbw = R_PointToAngle2(mo.x, mo.y, interest.x, interest.y)

						if vars.int_invm == true then
							P_TryCameraMove(cam,
							ease.linear(FRACUNIT/20, cam.x, mo.x-FixedMul(distinbtw, cos(angleinbw))+FixedMul(distinbtw, cos(angleinbw+ANGLE_90))),
							ease.linear(FRACUNIT/20, cam.y, mo.y-FixedMul(distinbtw, sin(angleinbw))+FixedMul(distinbtw, sin(angleinbw+ANGLE_90))))
						else
							P_TryCameraMove(cam,
							ease.linear(FRACUNIT/20, cam.x, interest.x+FixedMul(distinbtw, cos(angleinbw))+FixedMul(distinbtw, cos(angleinbw+ANGLE_90))),
							ease.linear(FRACUNIT/20, cam.y, interest.y+FixedMul(distinbtw, sin(angleinbw))+FixedMul(distinbtw, sin(angleinbw+ANGLE_90))))
						end
					end
				end
			end
		end

		vars.interest = nil
		vars.int_stay = false
		vars.int_move = false
		vars.int_invm = false
		vars.int_dist = 0
	end

	-- Following based on player on path
	if playerpaths then
		local point1, point2
		for _,v in pairs(playerpaths) do
			if not v.next then continue end
			local ndist = R_PointToDist2(mo.x, mo.y, v.x, v.y)
			local nextp = playerpaths[v.next]

			local cose_1x = cos(v.angle)
			local sine_1y = sin(v.angle)
			local cose_2x = cos(nextp.angle)
			local sine_2y = sin(nextp.angle)

			local line_side1 = P_PointOnLineSide(mo.x, mo.y, v.x+cose_1x, v.y+sine_1y, v.x-cose_1x, v.y-sine_1y)
			local line_side2 = P_PointOnLineSide(mo.x, mo.y, nextp.x+cose_2x, nextp.y+sine_2y, nextp.x-cose_2x, nextp.y-sine_2y)

			if (ndist < v.radius and not point1)
			or (line_side1 and not line_side2 and ndist < v.radius) then
				point1 = v
			end
		end

		if point1 then
			point2 = playerpaths[point1.next]
			local x, y = P_ClosestPointOnLine(mo.x, mo.y, point1.x, point1.y, point2.x, point2.y)
			local _, z = P_ClosestPointOnLine(mo.x, mo.z, point2.x, point2.z, point1.x, point1.z)

			cam.momx = 0
			cam.momy = 0
			cam.momz = 0

			P_TeleportCameraMove(cam,
			ease.linear(FRACUNIT/12, cam.x, x),
			ease.linear(FRACUNIT/12, cam.y, y),
			ease.linear(FRACUNIT/12, cam.z, z))
		end
	end

	-- Following based on path
	if vars.path_act == true then
		if vars.temppaths then
			local current_point = vars.temppaths[1]
			local dist3D = R_PointToDist2(0, 0, R_PointToDist2(cam.x, cam.y, current_point.x, current_point.y), current_point.z-cam.z)
			local speed = FixedDiv(current_point.speed, dist3D)

			cam.momx = 0
			cam.momy = 0
			cam.momz = 0

			P_TeleportCameraMove(cam,
			ease.linear(speed, cam.x, current_point.x),
			ease.linear(speed, cam.y, current_point.y),
			ease.linear(speed, cam.z, current_point.z))

			cam.angle = ease.linear(speed, cam.angle, current_point.angle)
			cam.aiming = ease.linear(speed, cam.aiming, current_point.aiming)

			if dist3D-FRACUNIT*2 < current_point.speed then
				if current_point.action then
					current_point.action()
				end

				table.remove(vars.temppaths, 1)
			end
		else
			vars.path_act = false
		end
	end


	--P_TryCameraMove(camera_t camera, fixed_t x, fixed_t y)

end

local last_point = nil

local function P_ResetCameraExtras(specific_cam)
	if specific_cam and cam_variables[specific_cam] then
		cam_variables[specific_cam] = camextras_class
	else
		for _,var in pairs(cam_variables) do
			var = camextras_class
		end
		interests = {}
		playerpaths = {}
		last_point = nil
	end
end

-- Reset
addHook("MapLoad", function()
	P_ResetCameraExtras()
end)

--@class {interests, [int_stay], [int_move], [int_invm], int_dist}
local function P_AddCameraInterest(mobj, int_stay, int_move, int_invm, int_dist)
	if not mobj then return end
	table.insert(interests, {mobj, int_stay or false, int_move or false, int_invm or false, int_dist or 0})
end

--@class {x, y, z, angle, aiming, speed, [action]},
--Note: please prefer cutaway camera, though it is likely it won't work out of box
--As of right now it was tested on normal camera, and it works...
--But due to distant limit before camera reset and bobbing, also camera's focus on player
--It seems to not be useable for cutscenes, but idea is there.
local function P_AddCameraStrictPathwayPoint(cam, x, y, z, angle, aiming, speed, action)
	if not (cam and x and y and z and angle and aiming and speed) then return end
	table.insert(cam_variables[cam].temppaths, {x = x, y = y, z = z, angle = angle, aiming = aiming, speed = speed, action = action})

	cam_variables[cam].path_act = true
end

--@class {x, y, z, direction, radius, next},
local function P_AddCameraPlayerPathwayPoint(x, y, z, direction, radius, next)
	if not (x and y and z and direction and radius) then return end
	table.insert(playerpaths, {x = x, y = y, z = z, angle = direction, radius = radius, next = next})

	return #playerpaths
end

-- This was test for interest points
--addHook("MobjThinker", function(mo)
--	if leveltime > 5 and not mo.gameremerald then
--		P_AddCameraInterest(mo, true, true, true, 800*FRACUNIT)
--		mo.gameremerald = true
--	end
--	P_CameraAdditions(camera2)
--end, MT_EMERALD2)

-- This was test for player-position based on pathway camera
--addHook("MobjThinker", function(mo)
--	if leveltime > 5 and not mo.gameremerald then
--		last_point = P_AddCameraPlayerPathwayPoint(mo.x, mo.y, mo.z, mo.angle, 800*FRACUNIT, last_point)
--		mo.gameremerald = true
--	end
--	--P_CameraAdditions(camera2)
--end, MT_EMERALD3)

-- This was test for strict waypoints for camera positions
--addHook("MobjThinker", function(mo)
--	if leveltime > 5 and not mo.gameremerald then
--		last_point = P_AddCameraStrictPathwayPoint(camera, mo.x, mo.y, mo.z, mo.angle, P_RandomRange(-16, 16)*ANG1, FRACUNIT*10, last_point)
--		mo.gameremerald = true
--	end
--	--P_CameraAdditions(camera2)
--end, MT_EMERALD4)

addHook("PlayerThink", function(p)
	P_CameraAdditions(camera, p)
end)

if MRCElibs then
	rawset(MRCElibs, "addCameraInterest", P_AddCameraInterest)
	rawset(MRCElibs, "addCameraWaypoint", P_AddCameraStrictPathwayPoint)
	rawset(MRCElibs, "addCameraHookpoint", P_AddCameraPlayerPathwayPoint)

	rawset(MRCElibs, "resetMRCECamera", P_ResetCameraExtras)
end