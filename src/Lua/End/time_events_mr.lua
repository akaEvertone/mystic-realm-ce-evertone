--
--	Mystic Realms Community Editions' OS-Time Events
--	Contributed by Ace Lite, Xian
--

if time_events then
	local funni = false

	-- should have been event and not it's own script, smh.
	time_events.add_headerset("Title_Sky", function(cdate, map)
		local titleSkies = { -- in order of emeralds
			9,     -- blue
			5005,  -- purple
			5004,  -- green
			499,   -- red
			5007,  -- light blue
			5008,  -- yellow
			5009   -- gray
		}

		-- Randomness is surprisingly REALLY fucking consistent,
		-- so we're going to steal a value that changes often.
		-- This way we can (kinda) randomize RNG without even
		-- playing the game. How professional of me.
		-- stjr pls fix
		--P_RandomKey(collectgarbage("count"))

		--xian here, v2.2.12 adds lua access to system time, so we'll use that to seed rng instead. still, gotta say, that was pretty neat usage of an otherwise useless function (in the case of srb2 at least)

		--Skydusk: Well, Doom's RNG is yes consistent for a reason. Demos, literally whole Doom's RNG is just LUT table.
		--So I would have been careful about this, but this usage is fine.
		--Not sure why it was boasted about anyway... considering it wasn't used in first place

		--local st = max((os.time(cdate) % FRACUNIT), 3)
		--print(modifiedgame)
		--P_RandomKey(st)

		P_SetupLevelSky(titleSkies[P_RandomKey(#titleSkies)+1], consoleplayer)
		time_events.ignore_default = true
	end)


	-- Primordial Abyss
	time_events.add_headerset("Pris_AP", function(cdate, map)
		if (cdate.day == 1 and cdate.month == 4) or funni then
			for s in sectors.iterate do
				for rover in s.ffloors() do
					if rover.sector.floorpic == "RAINBOWC"
					or rover.sector.floorpic == "DSBLOCK1"
					or rover.sector.floorpic == "DSBLOCK3" then
					--print("found one")
						if rover.alpha > 5 then
							rover.alpha = 5
							rover.blend = AST_ADD
						end
					end
				end
			end


			time_events.ignore_default = true
		end
	end)

	--
	--	RANDOM EVENTS FOR NO ONE
	--

	-- just VFZ

	time_events.add_headerset("VFZ_SkyColors", function(cdate, map)
		if not P_RandomKey(16) then
			P_SetupLevelSky(20000, nil)
		end
	end)


	-- global mobj level options

	--[[
	time_events.add_event(0, function(cdate, map)
		local global_scale = 0
		local global_color = 0
		local global_colorized = false
		local global_translation = nil

		if mapheaderinfo[map].mrce_mobjscale then
			global_scale = tonumber(mapheaderinfo[map].mrce_mobjscale)*FRACUNIT/100
		end

		if mapheaderinfo[map].mrce_mobjcolor then
			if skincolors[tonumber(_G[mapheaderinfo[map].mrce_mobjcolor])] then
				global_color = tonumber(_G[mapheaderinfo[map].mrce_mobjcolor])
			end
		end

		if mapheaderinfo[map].mrce_mobjcolorization and mapheaderinfo[map].mrce_mobjcolorization == 'true' then
			global_colorized = true
		end

		if mapheaderinfo[map].mrce_mobjtranslation then
			global_translation = tostring(mapheaderinfo[map].mrce_mobjtranslation)
		end

		if global_scale
		or global_color
		or global_colorized
		or global_translation then


			for nmobj in mobjs.iterate do
				if type(nmobj) ~= "userdata" then continue end

				if global_scale then
					nmobj.scale = $+global_scale
				end

				if nmobj.type ~= MT_PLAYER then

					if global_color then
						nmobj.color = global_color
					end

					if global_colorized then
						nmobj.colorized = global_colorized
					end

					if global_translation then
						nmobj.translation = global_translation
					end
				end
			end
		end
	end)
	--]]
end