local open_menu = false
local just_pressed = false
local text_mode = false

local holding_tab = false
local caps = false
local capslock = false

local scroll_const = 160 -- constant used for scrolling check
local maximum_offseted = 0 -- for scrolling check

local escape_select = "GT_COOP"
local escape_pos = 1
local maxtimer = 512
local timer = 0

local temp_buffers = {}

local contents = {}
local levels = {} -- for backtracking
local poslevels = {} -- for backtracking

-- Flags/Constants
local MM_NEVERTOUCH = 1
local MM_TEXTMODE = 2

-- Main Escape Menu Stuff

local enum_to_string = {
	[GT_COOP] = "GT_COOP",
}

-- Series of macros

local function close_menu()
	escape_select = "GT_COOP"
	temp_buffers = {} -- clean buffers
	levels = {}
	text_mode = false
	open_menu = false
	holding_tab = false
end

local function toggle_menu()
	escape_select = "GT_COOP"
	levels = {}
	open_menu = not (open_menu)
	just_pressed = true
	holding_tab = false

	-- change lenght
	maximum_offseted = 0
	for k, v in ipairs(contents[escape_select][escape_pos]) do
		maximum_offseted = $ + (v.offset_y or 0)
	end
end

local function change_menu(submenu, pos, returning)
	if not returning then
		table.insert(levels, escape_select)
		table.insert(poslevels, escape_pos)
		escape_pos = pos or 1
	end

	escape_select = tostring(submenu)
	just_pressed = true

	local pointer = contents[escape_select]

	-- change lenght
	maximum_offseted = 0
	for k, v in ipairs(pointer) do
		maximum_offseted = $ + (v.offset_y or 0)
	end

	-- check if there isn't entry function
	if pointer.entry then
		pointer.entry()
	end

	-- move to last touchable

	if pos then
		escape_pos = pos
	else
		while (pointer[escape_pos].flags & MM_NEVERTOUCH) do
			if escape_pos == #pointer then
				escape_pos = 1
			else
				escape_pos = min(escape_pos+1, #pointer)
			end
		end
	end
end

local function backtrack_menu()
	if #levels > 0 then
		change_menu(tostring(levels[#levels]), tonumber(poslevels[#poslevels]), true)
		table.remove(levels)
		table.remove(poslevels)
	else
		close_menu()
	end
end

local function scroll_menu(pointer, direction)
	if direction then
		if escape_pos == #pointer then
			escape_pos = 1
		else
			escape_pos = min(escape_pos+1, #pointer)
		end

		while (pointer[escape_pos].flags & MM_NEVERTOUCH) do
			if escape_pos == #pointer then
				escape_pos = 1
			else
				escape_pos = min(escape_pos+1, #pointer)
			end
		end
	else
		if escape_pos == 1 then
			escape_pos = #pointer
		else
			escape_pos = max(escape_pos-1, 1)
		end

		while (pointer[escape_pos].flags & MM_NEVERTOUCH) do
			if escape_pos == 1 then
				escape_pos = #pointer
			else
				escape_pos = max(escape_pos-1, 1)
			end
		end
	end
end

local function scroll_mini_menu(direction, vectors, vectorname, maxvalue)
	if direction then
		if vectors[vectorname] == maxvalue then
			vectors[vectorname] = 1
		else
			vectors[vectorname] = min(vectors[vectorname]+1, maxvalue)
		end
	else
		if vectors[vectorname] == 1 then
			vectors[vectorname] = maxvalue
		else
			vectors[vectorname] = max(vectors[vectorname]-1, 1)
		end
	end
end


--local macro_process = 0
--local current_macro = nil

--local get_to_options = {
	--[5] = {enable = 'escape'},
	--[4] = {enable = 'down', disable = 'escape'},
	--[3] = {enable = 'down'},
	--[2] = {enable = 'enter', disable = 'down'},
	--[1] = {disable = 'enter'},

--	[2] = {enable = 193},
--	[1] = {disable = 193},
--}

--local function run_macro()
--	if macro_process then
--		local step = current_macro[macro_process]
--
--		if step.enable then
--			gamekeydown[step.enable] = true
--			if gamekeydown[step.enable] then
--				print('pressed - '..step.enable)
--			end
--		end
--
--		if step.disable then
--			gamekeydown[step.disable] = false
--			if not gamekeydown[step.disable] then
--				print('disabled - '..step.disable)
--			end
--		end
--
--		macro_process = $-1
--	end
--end


-- Drawers

-- https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
local function V_DrawLineLow(v, x_0, y_0, x_1, y_1, color, thickness)
	local dx = x_1-x_0
	local dy = y_1-y_0
	local yi = 1
	if dy < 0 then
		yi = -1
		dy = -dy
	end
	local D = 2*dy - dx
	local y = y_0

	for x = x_0, x_1 do
		v.drawFill(x, y - thickness/2, 1, thickness, color)
		if D > 0 then
			y = y+yi
			D = D + (2*(dy - dx))
		else
			D = D + 2*dy
		end
	end
end

-- https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
local function V_DrawLineHigh(v, x_0, y_0, x_1, y_1, color, thickness)
	local dx = x_1-x_0
	local dy = y_1-y_0
	local xi = 1
	if dx < 0 then
		xi = -1
		dx = -dx
	end
	local D = 2*dx - dy
	local x = x_0

	for y = y_0, y_1 do
		v.drawFill(x - thickness/2, y, thickness, 1, color)
		if D > 0 then
			x = x+xi
			D = D + (2*(dx - dy))
		else
			D = D + 2*dx
		end
	end
end

local function V_DrawLine(v, x_0, y_0, x_1, y_1, color, thickness)
	if abs(y_1 - y_0) < abs(x_1 - x_0) then
		if x_0 > x_1 then
			V_DrawLineLow(v, x_1, y_1, x_0, y_0, color, thickness)
		else
			V_DrawLineLow(v, x_0, y_0, x_1, y_1, color, thickness)
		end
	else
		if y_0 > y_1 then
			V_DrawLineHigh(v, x_1, y_1, x_0, y_0, color, thickness)
		else
			V_DrawLineHigh(v, x_0, y_0, x_1, y_1, color, thickness)
		end
	end
end

--https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
--https://stackoverflow.com/questions/27755514/circle-with-thickness-drawing-algorithm (M Oehm)
local function V_DrawLine_x(v, x_1, x_2, y, color)
	v.drawFill(x_1, y, x_2-x_1, 1, color)
end

local function V_DrawLine_y(v, x, y_1, y_2, color)
	v.drawFill(x, y_1, 1, y_2-y_1, color)
end

local function V_DrawThickCircle(v, x_center, y_center, inner, outer, color)
	local x_o = outer
	local x_i = inner
	local y = 0
	local erro = 1 - x_o
	local erri = 1 - x_i

	local bandaid = 3*(outer-inner)/4
	v.drawFill(x_center - 5*x_o/7, y_center - 5*x_o/7, bandaid, bandaid, color)

	while (x_o >= y) do
		V_DrawLine_x(v, x_center + x_i, x_center + x_o, y_center + y, color)
		V_DrawLine_y(v, x_center + y, y_center + x_i, y_center + x_o, color)
		V_DrawLine_x(v, x_center - x_o, x_center - x_i, y_center + y, color)
		V_DrawLine_y(v, x_center - y, y_center + x_i, y_center + x_o, color)
		V_DrawLine_x(v, x_center - x_o, x_center - x_i, y_center - y, color)
		V_DrawLine_y(v, x_center - y, y_center - x_o, y_center - x_i, color)
		V_DrawLine_x(v, x_center + x_i, x_center + x_o, y_center - y, color)
		V_DrawLine_y(v, x_center + y, y_center - x_o, y_center - x_i, color)

		y = $+1

		if erro < 0 then
			erro = $+2*y+1
		else
			x_o = $-1
			erro = $+2*(y-x_o+1)
		end

		if (y > inner) then
			x_i = y
		else
			if erri < 0 then
				erri = $+2*y+1
			else
				x_i = $-1
				erri = $+2*(y-x_i+1)
			end
		end
	end
end

local achivement_list = {}
local function relevant_achivement_drawer(v, p, pointer, offset, item)
	local achivement = item.var1
	local unlocked = item.var2
	local sprite = achivement.hudsprite or "TABEMBICON"
	local color = v.getColormap(TC_DEFAULT, achivement.color)
	local patch = v.cachePatch(sprite)

	if pointer[escape_pos] == item then
		v.drawFill(20, offset-16, 320, 24, 118|V_SNAPTOLEFT|V_50TRANS)
	end



	if unlocked then
		MRCElibs.drawFakeHRotation(v, 20*FRACUNIT, offset*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT, patch, V_SNAPTOLEFT, color)
		v.drawString(40, offset-16, '\x8B'..(achivement.name or ''), V_SNAPTOLEFT)
		v.drawString(40, offset-8, '\x8A'..(achivement.desc or ''), V_SNAPTOLEFT, "thin")
		v.drawString(40, offset, '\x8A'..(achivement.tips or ''), V_SNAPTOLEFT, "thin")
	else
		MRCElibs.drawFakeHRotation(v, 20*FRACUNIT, offset*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT, patch, V_SNAPTOLEFT, v.getColormap(TC_DEFAULT, 0, "MRCEHUDBG"))
		v.drawString(40, offset-16, '\x86'..'???', V_SNAPTOLEFT, "thin")
		v.drawString(40, offset, (achivement.tips or ''), V_SNAPTOLEFT, "thin")
	end
end

local function unrelv_achivement_drawer(v, p, pointer, offset, item)
	local achivement = item.var1
	local unlocked = item.var2
	local sprite = achivement.hudsprite or "TABEMBICON"
	local color = v.getColormap(TC_DEFAULT, achivement.color)
	local patch = v.cachePatch(sprite)

	if unlocked then
		MRCElibs.drawFakeHRotation(v, 20*FRACUNIT, offset*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT, patch, V_SNAPTOLEFT, color)
		v.drawString(40, offset-16, '\x8B'..(achivement.name or ''), V_SNAPTOLEFT)
		v.drawString(40, offset-8, '\x8A'..(achivement.desc or ''), V_SNAPTOLEFT, "thin")
		v.drawString(40, offset, '\x8A'..(achivement.tips or ''), V_SNAPTOLEFT, "thin")
	else
		MRCElibs.drawFakeHRotation(v, 20*FRACUNIT, offset*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT, patch, V_SNAPTOLEFT, v.getColormap(TC_DEFAULT, 0, "MRCEHUDBG"))
		v.drawString(40, offset-16, '\x86'..'???', V_SNAPTOLEFT, "thin")
		v.drawString(40, offset, (achivement.tips or ''), V_SNAPTOLEFT, "thin")
	end
end

local function text_buff_drawer(v, p, pointer, offset, item)
	local buffer = ""..tostring(temp_buffers[item.var1])
	local lenght = item.var2*4
	local selected = pointer[escape_pos] == item

	if ((item.var2 and string.len(buffer) < item.var2) or not item.var2) and selected and (leveltime % 8 / 4) then
		buffer = $..'_'
	end

	v.drawFill(160-lenght-2, 18 + offset, lenght*2+4, 10, text_mode and 118 or 31)
	v.drawString(160-lenght, 20 + offset, buffer)
end

local initial_cache = false
local parts = {}

local function drawing_window(v, x, y, width, height, flags)
	local seg_dim = 8

	if not initial_cache then
		for k, pv in ipairs({4, 5, 6, 7, 8, 9, "A", "B", "C"}) do
			parts[k] = v.cachePatch("MRCEMENUBG"..pv)
		end
		initial_cache = true
	end

	v.drawStretched(
	(x+seg_dim) * FRACUNIT,
	(y+seg_dim) * FRACUNIT,
	(width-seg_dim)*FRACUNIT/seg_dim,
	(height-seg_dim)*FRACUNIT/seg_dim,
	parts[4], flags)

	local x_end = max((width/seg_dim)-2, 0)

	for indx = 0, x_end do
		local actual_x = x + indx*seg_dim
		local top_gpx = parts[2]
		local und_gpx = parts[6]

		if indx == 0 then
			top_gpx = parts[1]
			und_gpx = parts[5]
		elseif indx == x_end then
			top_gpx = parts[3]
			und_gpx = parts[7]
		end

		v.draw(actual_x, y, top_gpx, flags)
		v.draw(actual_x, y+height, und_gpx, flags)
	end

	local y_end = y + height - seg_dim

	for acty = y + seg_dim, y_end, seg_dim do
		v.draw(x, acty, parts[8], flags)
		v.draw(x+width-2*seg_dim, acty, parts[9], flags)
	end
end


local function escape_menu_drawer(v, p, pointer)
	local offset = 0
	local skew_x = 0

	v.draw(467, -80, v.cachePatch("MRCEMENUBG1"), V_SNAPTORIGHT)

	for k,item in ipairs(pointer) do
		offset = $ + item.offset_y
		skew_x = $ - 14

		if just_pressed then
			item.offset_x = 0
		end

		if k == escape_pos then
			item.offset_x = min(item.offset_x+2, 6)
			v.draw(235 - item.offset_x + skew_x, 52 + offset, v.cachePatch("MRCEMENUBUT2"), V_SNAPTORIGHT)
			v.drawString(235 - item.offset_x + skew_x, 48 + offset, '\x82'..item.name, V_SNAPTORIGHT, "center")
		else
			item.offset_x = max(item.offset_x-2, 0)
			v.draw(235 - item.offset_x + skew_x, 52 + offset, v.cachePatch("MRCEMENUBUT1"), V_SNAPTORIGHT)
			v.drawString(235 - item.offset_x + skew_x, 48 + offset, item.name, V_SNAPTORIGHT, "center")
		end
	end
end

local function get_base(pointer, position)
	local index = 1
	local num = 0
	while (pointer[index] and index <= position) do
		num = $ - pointer[index].offset_y
		index = $+1
	end
	return num
end

local function generic_background(v)
	local patch = v.cachePatch("MRCEMENUBG2")

	local scroll_y = (leveltime % patch.height) - patch.height
	local height = v.height()
	local width = v.width()
	local scalex = v.dupx()

	v.drawFill(0, 0, width/scalex-150, height/scalex, 117|V_SNAPTOLEFT)

	v.drawFill(width/scalex-150, 0, width/scalex-150, height/scalex, 117|V_SNAPTOTOP|V_SNAPTORIGHT)

	while(scroll_y < height) do
		v.draw(160-50, scroll_y, patch, V_SNAPTOTOP)
		v.draw(160+50, scroll_y, patch, V_FLIP|V_SNAPTOTOP)

		scroll_y = $ + patch.height
	end
end

local function generic_menu_drawer(v, p, pointer)
	local offset = scroll_const < maximum_offseted and 50+get_base(pointer, escape_pos) or 0

	if pointer.bg then
		pointer.bg(v)
	end

	for k,item in ipairs(pointer) do
		offset = $ + item.offset_y

		if item.drawer then
			item.drawer(v, p, pointer, offset, item)
		else
			if k == escape_pos then
				v.draw(160, offset, v.cachePatch("MRCEMENUBUT2"))
				v.drawString(160, offset-8, '\x82'..item.name, 0, "center")
			else
				v.draw(160, offset, v.cachePatch("MRCEMENUBUT1"))
				v.drawString(160, offset-8, item.name, 0, "center")
			end

			if item.cvar then
				local cvar = item.cvar
				local val = tostring(cvar.string)
				v.drawString(160, offset, ((val == cvar.defaultvalue and '\140' or '')..'< '..val..' >'), 0, "center")
			end
		end
	end

	if pointer.name then
		local name = pointer.name

		v.draw(160, 0, v.cachePatch("MRCEMENUBG3"), V_SNAPTOTOP)
		v.drawString(160, 2, '\x8B'..name, 0, "center")
	end
end

-- Menu Contents

contents = {
	["GT_COOP"] = {
		drawer = escape_menu_drawer,

		{name = "Achievement Hints", func = function()
			change_menu("ACHIVEMENTSHINTS")
		end, offset_y = 0, offset_x = 0, flags = 0},

		{name = "Achievement List", func = function()
			change_menu("ACHIVEMENTS")
		end, offset_y = 32, offset_x = 0, flags = 0},

		{name = "Passwords", func = function()
			temp_buffers["PASSWORD"] = "" -- open up buffer
			change_menu("PASSWORD")
		end, offset_y = 32, offset_x = 0, flags = 0},

		{name = "MRCE Settings", func = function()
			change_menu("SETTINGS")
		end, offset_y = 32, offset_x = 0, flags = 0},

		{name = "Exit Menu", func = function()
			close_menu()
		end, offset_y = 32, offset_x = 0, flags = 0},
	},

	["ACHIVEMENTSHINTS"] = {
		drawer = generic_menu_drawer,
		name = "HINTS",

		entry = function() -- setup achivement hints
			local keyword = mapheaderinfo[gamemap].keywords
			local array = MRCE_Achivements.searchByCategory(string.upper(keyword))
			local saves = MRCE_Unlock.getSaveData().achivements

			-- clean the table
			while(contents['ACHIVEMENTSHINTS'][1]) do
				table.remove(contents['ACHIVEMENTSHINTS'])
			end

			local defaultoffset = 35
			local offset_y = 24

			if array then
				for i = 1, #array do
					local key = array[i]
					local achivement = MRCE_Achivements[key]
					if achivement.flags & 1 and not saves[key] then continue end -- hidden achivements won't appear until they are unlocked
					table.insert(contents['ACHIVEMENTSHINTS'], 	{name = "ACHIVEMENT", offset_y = i == 1 and defaultoffset or offset_y, drawer = relevant_achivement_drawer, flags = 0, var1 = achivement, var2 = saves[key]})
				end
			end

			-- exit
			table.insert(contents['ACHIVEMENTSHINTS'], 	{name = "Return To Main", func = function()
			backtrack_menu()
			end, offset_y = 24, flags = 0})

		end,

		{name = "Return To Main", func = function()
			backtrack_menu()
		end, offset_y = 56, flags = 0},
	},

	["ACHIVEMENTS"] = {
		drawer = generic_menu_drawer,
		name = "ACHIVEMENTS",

		entry = function() -- setup achivement list
			local pointerx = contents["ACHIVEMENTS"]

			local array = MRCE_Achivements
			local saves = MRCE_Unlock.getSaveData().achivements

			-- To make sure procentage is correct
			MRCE_Achivements.recount()

			local last_type = ""
			local index = 0
			local num_achivements = 0
			achivement_list = {}

			if array then
				for i, achivementobj in ipairs(array) do
					if achivementobj.flags & 1 and not saves[i] then continue end -- hidden achivements won't appear until they are unlocked
					local typexyv = tostring(achivementobj.typein)
					if achivementobj.inlevel then
						typexyv = mapheaderinfo[achivementobj.inlevel].lvlttl
					end

					local new_list = (string.upper(typexyv) ~= string.upper(last_type))

					if new_list or (num_achivements > 1 and not (num_achivements % 8)) then
						if new_list then
							num_achivements = 0
						end

						last_type = typexyv
						index = $+1

						achivement_list[index] = {name = typexyv}
					end

					table.insert(achivement_list[index], {achivement = achivementobj, unlock = saves[i]})
					num_achivements = $+1
				end
			end

			pointerx[1].local_select_x = 1
			pointerx[1].local_select_y = 1
		end,

		{name = "ACHIVEMENT_HANDLER", offset_y = 32, flags = 0, keyhandler = function(key)
			local upx, upy = input.gameControlToKeyNum(GC_FORWARD)
			local downx, downy = input.gameControlToKeyNum(GC_BACKWARD)

			local leftx, lefty = input.gameControlToKeyNum(GC_STRAFELEFT)
			local lefta, leftb = input.gameControlToKeyNum(GC_TURNLEFT)

			local rightx, righty = input.gameControlToKeyNum(GC_STRAFERIGHT)
			local righta, rightb = input.gameControlToKeyNum(GC_TURNRIGHT)


			local vectors = contents['ACHIVEMENTS'][1]

			if (key.num == leftx or key.num == lefty or key.num == lefta or key.num == leftb
			or key.name == "left") then
				scroll_mini_menu(false, vectors, 'local_select_x', 8)
				return true
			end

			if (key.num == rightx or key.num == righty or key.num == righta or key.num == rightb
			or key.name == "right") then
				scroll_mini_menu(true, vectors, 'local_select_x', 8)
				return true
			end

			if key.num == downx or key.num == downy
			or key.name == "down" then
				scroll_mini_menu(true, vectors, 'local_select_y', #achivement_list)
				return true
			end

			if key.num == upx or key.num == upy
			or key.name == "up" then
				scroll_mini_menu(false, vectors, 'local_select_y', #achivement_list)
				return true
			end

		end,
		drawer = function(v, p, pointer, offset, item)
			local list = achivement_list
			if not list then return end

			local selected_x = item.local_select_x
			local selected_y = item.local_select_y

			local list_max = #list
			local hitblock = max(min(selected_y, list_max-5), 0)
			local last_cat = ''

			local internal_scale = v.dupx()
			drawing_window(v, 0, 130, (v.width()/internal_scale)+8, (v.height()/internal_scale)-65, V_SNAPTOLEFT)

			local progress = MRCE_Achivements.progress()
			v.drawString(310, 20, string.format("%f", progress*100)..'%', V_SNAPTORIGHT, "right")

			v.drawFill(75 + 30*selected_x - 12, 30 + offset + max(selected_y-(list_max-5), 0) * 12 - 11, 24, 12, 116|V_SNAPTOLEFT|V_40TRANS)

			for y = hitblock, hitblock+5 do
				local achieve = achivement_list[y]
				if not achieve then continue end
				local real_y = (y-hitblock) * 12

				local grayout = v.getColormap(TC_DEFAULT, 0, "MRCEHUDBG")
				local arrayname = tostring(achieve.name)

				if y == selected_y then
					v.drawFill(0, 20 + offset + real_y, 75, 8, 118|V_SNAPTOLEFT|V_50TRANS)
				end

				if last_cat ~= arrayname then
					v.drawString(0, 20 + offset + real_y, arrayname, V_SNAPTOLEFT)
					last_cat = arrayname
				end
				for x = 1, #achieve do
					local achv_local = achieve[x]
					local sprite = achv_local.achivement.hudsprite or "TABEMBICON"
					local patch = v.cachePatch(sprite)

					if achv_local.unlock then
						local color = v.getColormap(TC_DEFAULT, achv_local.achivement.color)
						MRCElibs.drawFakeHRotation(v, (75 + 30*x)*FRACUNIT, (30 + offset + real_y)*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT-FRACUNIT/3, patch, V_SNAPTOLEFT, color)
					else
						MRCElibs.drawFakeHRotation(v, (75 + 30*x)*FRACUNIT, (30 + offset + real_y)*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT-FRACUNIT/3, patch, V_SNAPTOLEFT, grayout)
					end
				end
			end

			if list[selected_y] and list[selected_y][selected_x] then
				local achivement = list[selected_y][selected_x]
				unrelv_achivement_drawer(v, p, pointer, 130 + offset, {var1 = achivement.achivement, var2 = achivement.unlock})
			end
		end, local_select_x = 1, local_select_y = 1},
	},

	["PASSWORD"] = {
		drawer = generic_menu_drawer,
		--bg = generic_background,
		name = "PASSWORDS",

		{name = '\x8B'.."PASSWORD", offset_y = 80, flags = MM_TEXTMODE, func = function()
			COM_BufInsertText(consoleplayer, "mrsecret "..(temp_buffers["PASSWORD"] or ""))
			temp_buffers["PASSWORD"] = ""
		end, drawer = text_buff_drawer, var1 = "PASSWORD", var2 = 16},

		{name = "Return To Main", func = function()
			backtrack_menu()
		end, offset_y = 52, flags = 0},
	},


	["SETTINGS"] = {
		drawer = generic_menu_drawer,
		--bg = generic_background,
		name = "MRCE SETTINGS",

		{name = "Remove Borealis", offset_y = 32, flags = 0, cvar = CV_FindVar("mr_mfz1boost")},

		{name = "Force Season", offset_y = 32, flags = 0, cvar = CV_FindVar("force_time_event")},

		{name = "Powerup Display Size", offset_y = 32, flags = 0, cvar = CV_FindVar("powerupdisplay_size")},

		{name = "Corona Size", offset_y = 32, flags = 0, cvar = CV_FindVar("corona_size")},

		{name = "Corona Strength", offset_y = 32, flags = 0, cvar = CV_FindVar("cv_corona_strength")},

		{name = "Corona Offset", offset_y = 32, flags = 0, cvar = CV_FindVar("cv_corona_offset")},

		{name = "Return To Main", func = function()
			backtrack_menu()
		end, offset_y = 40, flags = 0},
	},
}

local sys1, sys2
local emblemlist = {}
local numembs = 0


addHook("MapLoad", function()
	emblemlist = {}
	numembs = 0
	sys1, sys2 = input.gameControlToKeyNum(GC_SYSTEMMENU)
end)

addHook("KeyUp", function(key)
	if (key.name == "lshift" or key.name == "rshift") and caps and not capslock then
		caps = false
	end
end)

 -- Considering it doesn't activates on mobile, ironically this should be just fine. -- I hope, that's just assumption
addHook("KeyDown", function(key)
	--print(key.num)
	--if macro_process and current_macro then
	--	return false
	--end

	if gamestate ~= GS_LEVEL
	or gametype ~= GT_COOP
	or gamemap == 99
	or menuactive
	or multiplayer  -- multiplayer for now
	or not mapheaderinfo[gamemap].mysticrealms then
		if open_menu then
			close_menu()
		end
		return
	end

	if not open_menu and holding_tab and (key.num == sys1 or key.num == sys2 or key.name == "escape") then
		toggle_menu()
		return true
	end

	--local t2 = sys2 or false

	--print(sys1..' '..t2..' '..key.num)
	--if (key.num == sys1 or key.num == sys2 or key.name == "escape") and not open_menu and not key.repeated then
	--	toggle_menu()
	--	return true
	--end

	if text_mode then
		local pointer = contents[escape_select]

		if not (open_menu and pointer[escape_pos].flags & MM_TEXTMODE) then
			text_mode = false
			return true
		end

		if (key.name == "escape") then
			text_mode = false
			return true
		end

		if (key.name == "lshift" or key.name == "rshift") then
			-- Dear... upper case letters...
			caps = true
			return true
		end

		if (key.name == "capslock") then
			-- GUYS WE INVENTED CAPSLOCK
			capslock = not (capslock)
			caps = TBS_Menu.capslock
		end

		if (key.name == "delete") then
			-- delete this or else...
			temp_buffers[pointer[escape_pos].var1] = ""
		end

		if (key.name == "backspace") then
			-- delete this or else...
			temp_buffers[pointer[escape_pos].var1] = string.sub(temp_buffers[pointer[escape_pos].var1], 1, -2)
		end

		if input.keyNumPrintable(key.num) and key.num > 31 and key.num < 127 then
			local buffer = temp_buffers[pointer[escape_pos].var1]
			local limit = pointer[escape_pos].var2
			local chara = input.keyNumToName(caps and input.shiftKeyNum(key.num) or key.num)

			if key.num == 32 then
				chara = ' '
			end

			if limit and #buffer >= limit then return true end
			temp_buffers[pointer[escape_pos].var1] = $..chara
		end

		if key.name == "enter" and pointer[escape_pos].func then
			pointer[escape_pos].func()
			return true
		end

		return true
	elseif open_menu then
		local pointer = contents[escape_select]
		local upx, upy = input.gameControlToKeyNum(GC_FORWARD)
		local downx, downy = input.gameControlToKeyNum(GC_BACKWARD)

		local leftx, lefty = input.gameControlToKeyNum(GC_STRAFELEFT)
		local lefta, leftb = input.gameControlToKeyNum(GC_TURNLEFT)

		local rightx, righty = input.gameControlToKeyNum(GC_STRAFERIGHT)
		local righta, rightb = input.gameControlToKeyNum(GC_TURNRIGHT)

		local jumpx, jumpy = input.gameControlToKeyNum(GC_JUMP)
		--print(down1..' '..up1..' '..key.num)

		if pointer[escape_pos].keyhandler and pointer[escape_pos].keyhandler(key) then
			return true
		end

		if pointer[escape_pos].cvar then
			local item = pointer[escape_pos]
			local cvar = item.cvar

			if (key.num == leftx or key.num == lefty or key.num == lefta or key.num == leftb
			or key.name == "left") then
				CV_AddValue(cvar, -(item.var1 or 1))
			end

			if (key.num == rightx or key.num == righty or key.num == righta or key.num == rightb
			or key.name == "right") then
				CV_AddValue(cvar, item.var1 or 1)
			end
		end

		if key.num == downx or key.num == downy
		or key.name == "down" then
			scroll_menu(pointer, 1)
		end

		if key.num == upx or key.num == upy
		or key.name == "up" then
			scroll_menu(pointer, 0)
		end

		if key.num == jumpx
		or key.num == jumpy
		or key.name == "enter" then
			if pointer[escape_pos].flags & MM_TEXTMODE and key.name == "enter" then
				text_mode = true
			elseif pointer[escape_pos].func then
				pointer[escape_pos].func()
			else
				return true -- sound function will be there
			end
		end

		if (key.num == sys1 or key.num == sys2 or key.name == "escape") then
			backtrack_menu()
		end

		return true
	end
end)

local hackyjoysticktimer1, hackyjoysticktimer2 = 0, 0
local joyticdelay = 5 --when holding a direction how quickly should scrolling occur, in tics

addHook("PlayerThink", function(p)
	if not open_menu then return end
	if text_mode then return end
	local pointer = contents[escape_select]
	--p.powers[pw_nocontrol] = 3
	--p.powers[pw_flashing] = 1
	local vectors = contents['ACHIVEMENTS'][1]
	if (input.joyAxis(JA_STRAFE)) > 200 then
		hackyjoysticktimer2 = $ < 35 and $ + 1 or 1
	elseif (input.joyAxis(JA_STRAFE)) < -200 then
		hackyjoysticktimer2 = $ > -35 and $ - 1 or -1
	else
		hackyjoysticktimer2 = 0
	end

	if (input.joyAxis(JA_MOVE)) > 200 then --holding down makes positive values ironically
		hackyjoysticktimer1 = $ < 35 and $ + 1 or 1
	elseif (input.joyAxis(JA_MOVE)) < -200 then --holding up makes negative values
		hackyjoysticktimer1 = $ > -35 and $ - 1 or -1
	else
		hackyjoysticktimer1 = 0
	end

	if hackyjoysticktimer1 > 0 and not (hackyjoysticktimer1 % joyticdelay) then
		scroll_menu(pointer, 1)
		scroll_mini_menu(true, vectors, 'local_select_y', #achivement_list)
	elseif hackyjoysticktimer1 < 0 and not (hackyjoysticktimer1 % joyticdelay) then
		scroll_menu(pointer, 0)
		scroll_mini_menu(false, vectors, 'local_select_y', #achivement_list)
	end
	if hackyjoysticktimer2 > 0 and not (hackyjoysticktimer2 % joyticdelay) then
		scroll_mini_menu(true, vectors, 'local_select_x', 8)
	elseif hackyjoysticktimer2 < 0 and not (hackyjoysticktimer2 % joyticdelay) then
		scroll_mini_menu(false, vectors, 'local_select_x', 8)
	end


end)


COM_AddCommand("debug_mrce_escapemenu", function()
	toggle_menu()
end, COM_LOCAL)

local function fixedintmul(v, mul)
	return (v * mul) / FRACUNIT
end

local function draw_triangle(x1, x2, y1, y2, color, skip, side)
	local y_dif = abs(y2-y1)
	skip = skip-1 or 0

	if side then
		for i = 1, y_dif, 1-(skip) do
			v.drawFill(x2 - i, z2 - i, i, 1, color)
		end
	else
		for i = 1, y_dif, 1-(skip) do
			v.drawFill(x1, z2 - i, i, 1, color)
		end
	end
end

local tempstoptime = nil
local circle_bool = false
local circle_start = 0
local circle_end = FRACUNIT+FRACUNIT/4
local circlethrsh = FRACUNIT/32
local touch_timer = 0

local circle_scale = 0
local hud_disable = hud.disable
local hud_enable = hud.enable


hud.add(function(v)
	holding_tab = false
	if not multiplayer then
		circle_bool = true
		holding_tab = true
	end

	if mapheaderinfo[gamemap].mysticrealms then
		hud_disable("tokens")
		hud_disable("tabemblems")
	else
		hud_enable("tokens")
		hud_enable("tabemblems")
	end
end, "scores")

-- #################################
-- DIRECTLY COPY PASTED FROM NEWEMBLEMHUD
-- #################################

local UNCOLLECTED_EMB, UNCOLLECTED_EMB_WIDTH
local EMB_FLAGS = V_SNAPTOTOP|V_SNAPTORIGHT
local EMB_SCALE = FRACUNIT/3
local EMB_ANGLEOFFSET = ANGLE_90/32
local EMB_PIERANGE = ANGLE_90

addHook("MobjSpawn", function(mo)
	if multiplayer then return end
	mo.update = 6 -- 6 tics of update
end, MT_MRCEACHIVEMENT)

addHook("MobjSpawn", function(mo)
	if multiplayer then return end
	mo.update = 6 -- 6 tics of update
end, MT_EMBLEM)

addHook("MobjThinker", function(mo)
	if not mo.update then return end
	emblemlist[mo] = {rotation = 0, frame = mo.frame, sprite = mo.sprite, color = mo.color}
	mo.update = $-1
	if not mo.update then
		numembs = $+1
	end
end, MT_MRCEACHIVEMENT)

addHook("MobjThinker", function(mo)
	if not mo.update then return end
	emblemlist[mo] = {rotation = 0, frame = mo.frame, sprite = mo.sprite, color = mo.color}
	mo.update = $-1
	if not mo.update then
		numembs = $+1
	end
end, MT_EMBLEM)

addHook("TouchSpecial", function(mo)
	if not emblemlist[mo] then return end
	if not emblemlist[mo].rotation then
		emblemlist[mo].rotation = 360
	end
	emblemlist[mo].collected = true
	touch_timer = 64
end, MT_MRCEACHIVEMENT)

addHook("TouchSpecial", function(mo)
	if not emblemlist[mo] then return end
	if not emblemlist[mo].rotation then
		emblemlist[mo].rotation = 360
	end
	emblemlist[mo].collected = true
	touch_timer = 64
end, MT_EMBLEM)

-- #################################
-- END COPY PASTED FROM NEWEMBLEMHUD
-- #################################

--$GZDB_SKIP
local tk_x = (151+3) << FRACBITS -- base+new_offset

customhud.SetupItem("mrce_escapemenu", "mrce", function(v, p)
	if not mapheaderinfo[gamemap].mysticrealms then
		text_mode = false
		open_menu = false
		holding_tab = false
		return false
	end

	if not UNCOLLECTED_EMB then
		-- EMBLEM
		UNCOLLECTED_EMB = v.cachePatch("UNCEMB")
		UNCOLLECTED_EMB_WIDTH = (UNCOLLECTED_EMB.width / 6)*(10*FRACUNIT)
	end

	if circle_scale > circlethrsh then
		local y_pos, y_flip = 0, 1

		if mapheaderinfo[gamemap].mrce_emeraldstage then
			y_pos = 200
			y_flip = -1
		end


		V_DrawThickCircle(v, 320, y_pos, fixedintmul(74, circle_scale), fixedintmul(76, circle_scale), 0|V_SNAPTOTOP|V_SNAPTORIGHT)
		V_DrawThickCircle(v, 320, y_pos, 0, fixedintmul(72, circle_scale), 26|V_SNAPTOTOP|V_SNAPTORIGHT)

		local embcnt_scale = fixedintmul(34, circle_scale)
		local embdist_scale = fixedintmul(64, circle_scale)
		local pie_split = EMB_PIERANGE/(numembs+1)
		local embindex = 1

		local max_c = MRCE_Achivements
		local count = max_c.unlocked

		TBSlib.fontdrawerInt(v, "MRCEGFNT", 316-embcnt_scale, y_pos+(embcnt_scale-24)*y_flip, 'TAB+ESC', EMB_FLAGS, nil, "left", -1)
		TBSlib.fontdrawerInt(v, "MRCEHU2FNT", 320-embcnt_scale, y_pos+(embcnt_scale-8)*y_flip, tostring(count).."/"..#max_c, EMB_FLAGS, nil, "left", -1)

		for origin,data in pairs(emblemlist) do
			local x = (320 << FRACBITS) - embdist_scale * cos(embindex * pie_split - EMB_ANGLEOFFSET)
			local y = y_pos + y_flip * embdist_scale * sin(embindex * pie_split - EMB_ANGLEOFFSET)

			if data.collected or (origin and (origin.flags2 & MF2_DONTDRAW or origin.frame & FF_TRANSMASK or origin.state == S_INVISIBLE)) or not origin then
				local emblempatch = v.getSpritePatch(data.sprite, data.frame & FF_FRAMEMASK)
				local emblemcolor = v.getColormap(TC_DEFAULT, data.color)

				MRCElibs.drawFakeHRotation(v, x, y, data.rotation*ANG1, EMB_SCALE, emblempatch, EMB_FLAGS, emblemcolor)
				if data.rotation then
					data.rotation = $-18
				end

				data.collected = true
			else
				v.drawScaled(x, y, EMB_SCALE, UNCOLLECTED_EMB, EMB_FLAGS, nil)
			end

			embindex = $+1
		end
	end

	if circle_bool then
		circle_scale = min(ease.linear(FRACUNIT/6, circle_scale, circle_end), FRACUNIT)
	elseif touch_timer then
		touch_timer = $ - 1
		circle_scale = min(ease.linear(FRACUNIT/6, circle_scale, circle_end), FRACUNIT)
	else
		circle_scale = max(ease.linear(FRACUNIT/6, circle_scale, circle_start), 0)
	end

	-- menu
	if open_menu then
		local pointer = contents[escape_select]
		v.fadeScreen(31, 6)

		pointer.drawer(v, p, pointer)
		just_pressed = false
	end

	circle_bool = false
	holding_tab = false
	--run_macro()
end, "game")